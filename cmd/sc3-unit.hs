import System.Environment {- base -}

import qualified Sound.Osc.Fd as Fd {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Fd.Udp {- hosc -}
import qualified Sound.Sc3 as S {- hsc3 -}

import qualified Sound.Midi.Osc as M {- midi-osc -}
import qualified Sound.Midi.Osc.Server as M {- midi-osc -}
import Sound.Midi.Type {- midi-osc -}

type Send_f = Fd.Udp.Udp -> Int -> Int -> IO ()

send_ctl :: Send_f
send_ctl s_fd d1 d2 =
  let d2_ = fromIntegral d2 / 127.0
  in print (d1, d2_) >> Fd.sendMessage s_fd (S.c_set1 d1 d2_)

send_buf :: Int -> Send_f
send_buf b s_fd d1 d2 =
  let d2_ = fromIntegral d2 / 127.0
  in print (d1, d2_) >> Fd.sendMessage s_fd (S.b_set1 b d1 d2_)

type Proc_f = Fd.Udp.Udp -> Channel_Voice_Message Int -> IO ()

-- | Put (0,1) values on Cc-indexed Sc3 control buses.
ctl :: Send_f -> Proc_f
ctl f s_fd d =
  case d of
    Control_Change _ d1 d2 -> f s_fd d1 d2
    _ -> return ()

-- | Put (0,1) velocity on Mnn-indexed Sc3 control buses.
mnn :: Send_f -> Proc_f
mnn f s_fd d =
  case d of
    Note_Off _ d1 _ -> f s_fd d1 0
    Note_On _ d1 d2 -> f s_fd d1 d2
    _ -> return ()

sc3_unit :: Proc_f -> IO ()
sc3_unit f = do
  sc3_fd <- Fd.Udp.openUdp "127.0.0.1" 57110 -- scsynth
  let recv_f = f sc3_fd . M.osc_to_cvm
  M.midi_osc_processor recv_f (Fd.close sc3_fd)

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["ctl", "ctl"] -> sc3_unit (ctl send_ctl)
    ["ctl", "buf", b] -> sc3_unit (ctl (send_buf (read b)))
    ["mnn", "ctl"] -> sc3_unit (mnn send_ctl)
    ["mnn", "buf", b] -> sc3_unit (mnn (send_buf (read b)))
    _ -> error "sc3-unit: {ctl|mnn} {ctl|buf id}"
