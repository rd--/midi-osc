import Control.Concurrent {- base -}
import Control.Monad {- base -}
import Data.IORef {- base -}
import qualified Data.Map as M {- containers -}
import System.Environment {- base -}
import System.IO {- base -}

import Sound.Osc.Datum {- hosc -}
import qualified Sound.Osc.Datum.Normalise as Datum.Normalise {- hosc -}
import qualified Sound.Osc.Transport.Fd as Fd {- hosc -}
import qualified Sound.Osc.Transport.Fd.Udp as Fd.Udp {- hosc -}
import Sound.Osc.Packet {- hosc -}


import Sound.Sc3 {- hsc3 -}

import qualified Sound.Sc3.Lang.Math.Warp as Warp {- hsc3-lang -}

import qualified Sound.Midi.Osc as M {- midi-osc -}
import qualified Sound.Midi.Osc.Sc3 as M {- midi-osc -}
import qualified Sound.Midi.Osc.Server as M {- midi-osc -}
import qualified Sound.Midi.Type as M {- midi-osc -}

type Opt = Bool
type PR = (Int, String, Double -> Double, String)

-- ((node-id,synth-name),(cc,param))
type St = (M.Map Int String, M.Map Int PR)

to_warp :: (Eq t, RealFrac t, Floating t) => String -> t -> t -> t -> t
to_warp nm l r =
  case Warp.warpNamed nm of
    Nothing -> error (show ("to_warp", nm))
    Just w -> w l r Warp.W_Map

un_param :: M.Param -> (Int, PR)
un_param (M.Param cc k nm l r w u) = (cc, (k, nm, to_warp w l r, u))

init_param :: [M.Param] -> M.Map Int PR
init_param = M.fromList . map un_param

add_param :: M.Param -> M.Map Int PR -> M.Map Int PR
add_param p = let (k, v) = un_param p in M.insert k v

recv_config :: Message -> St -> St
recv_config m (sy, pr) =
  case Datum.Normalise.normalise_message m of
    Message "/clear" [] -> (M.empty, M.empty)
    Message "/synth" [Int64 k, AsciiString nm] ->
      (M.insert (fromIntegral k) (ascii_to_string nm) sy, pr)
    Message
      "/param"
      [ Int64 cc
        , Int64 k
        , AsciiString nm
        , Double l
        , Double r
        , AsciiString w
        , AsciiString u
        ] ->
        let k' = fromIntegral k
            cc' = fromIntegral cc
            nm' = ascii_to_string nm
            w' = ascii_to_string w
            u' = ascii_to_string u
        in (sy, add_param (M.Param cc' k' nm' l r w' u') pr)
    _ -> (sy, pr)

display :: [String] -> IO ()
display = putStrLn . unwords

-- > map maybe_show [Just 1,Nothing] == ["1",""]
maybe_show :: Show a => Maybe a -> String
maybe_show = maybe "" show

ctl :: Opt -> St -> Fd.Udp.Udp -> M.Channel_Voice_Message Int -> IO ()
ctl oi (sy, pr) fd d =
  case d of
    M.Control_Change _ c x ->
      let c_ = fromIntegral c + if oi then 1 else 0
          x_ = fromIntegral x / 127.0
      in case M.lookup c_ pr of
          Just (k, p, f, u) ->
            print (c_, x_)
              >> display [show c_, show k, maybe_show (M.lookup k sy), p, double_pp 3 (f x_), u]
              >> Fd.sendMessage fd (n_set1 k p (f x_))
          Nothing -> print ("no-config", c_) >> return ()
    _ -> print ("non-control message", d) >> return ()

start_server :: IORef St -> IO ThreadId
start_server st_ref =
  let f fd = do
        m <- Fd.recvMessage fd
        print ("server", m)
        maybe (return ()) (modifyIORef st_ref . recv_config) m
      t = Fd.Udp.udpServer "127.0.0.1" 57300
  in forkIO (Fd.withTransport t (forever . f))

help :: [String]
help =
  [ "midi-osc-sc3 [-1] [csv-file...]"
  , "  -1 = one-indexed"
  ]

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  a <- getArgs
  let (oi, pr) = case a of
        ["-h"] -> error (unlines help)
        "-1" : a' -> (True, map M.parse_param_csv a')
        _ -> (False, map M.parse_param_csv a)
  s_fd <- Fd.Udp.openUdp "127.0.0.1" 57110 -- scsynth
  st_ref <- newIORef (M.empty, init_param pr)
  thr <- start_server st_ref
  let proc_f m = readIORef st_ref >>= \st -> ctl oi st s_fd (M.osc_to_cvm m)
  M.midi_osc_processor proc_f (Fd.close s_fd >> killThread thr)
