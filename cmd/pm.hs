import Control.Concurrent {- base -}
-- import Data.Foldable {- base -}

import Data.List {- base -}

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Data.Map as Map {- containers -}

import qualified Sound.Osc.Fd as Osc {- hosc -}
import qualified Sound.Sc3 as Sc3 {- hosc -}

import qualified Sound.Midi.Common as Common {- midi-osc -}
import qualified Sound.Midi.Csv as Csv {- midi-osc -}
import qualified Sound.Midi.Pm as Pm {- midi-osc -}
import qualified Sound.Midi.Pp as Pp {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

import qualified Music.Theory.Array.Text as T {- hmt-base -}
import qualified Music.Theory.Math.Convert as T {- hmt-base -}
import qualified Music.Theory.Opt as T {- hmt-base -}

-- * Osc ; Cc ; Rel

-- | Index -> Cc.  0-indexed address of ctl, relative to ix0.
type Osc_Cc_St = Map.Map Int Float

-- | (number-of-channels,number-of-controls-per-channel,cc-ix-zero,cc-center,cc-increment)
type Osc_Cc_Opt = (Midi.Byte, Midi.Byte, Sc3.Bus_Id, Int, Float)

-- > osc_cc_calc_extent (16,16) == 256
osc_cc_calc_extent :: (Midi.Byte, Midi.Byte) -> Int
osc_cc_calc_extent (n_ch, n_cc) = Midi.byte_to_int n_ch * Midi.byte_to_int n_cc

-- > osc_cc_get ("127.0.0.1",57110) (16,16,11000)
osc_cc_get :: Osc.OscSocketAddress -> (Midi.Byte, Midi.Byte, Int) -> IO [Float]
osc_cc_get addr (n_ch, n_cc, ix0) =
  let ix = (ix0, osc_cc_calc_extent (n_ch, n_cc))
  in Sc3.withSc3At addr (Sc3.c_getn1_data ix)

osc_cc_init_map :: Osc.OscSocketAddress -> Osc_Cc_Opt -> IO Osc_Cc_St
osc_cc_init_map addr (n_ch, n_cc, ix0, _, _) = do
  r <- osc_cc_get addr (n_ch, n_cc, ix0)
  return (Map.fromList (zip [0 ..] r))

-- | 0-indexed address of ctl (relative to ix0).  n_cc = number-of-controls-per-channel.
osc_cc_calc_index :: Num n => n -> (n, n) -> n
osc_cc_calc_index n_cc (ch, cc) = (ch * n_cc) + cc

osc_cc_st_edit :: Osc_Cc_Opt -> Midi.Cvm3 Midi.Byte -> Osc_Cc_St -> (Osc_Cc_St, Maybe (Int, Float))
osc_cc_st_edit (n_ch, n_cc, ix0, cntr, incr) (st, d1, d2) m =
  case Midi.status_ch_ty st of
    (ch, 0xB) ->
      if ch >= n_ch || d1 >= n_cc
        then (m, Nothing)
        else
          let ix = osc_cc_calc_index (Midi.byte_to_int n_cc) (Midi.byte_to_int ch, Midi.byte_to_int d1)
              x = Map.findWithDefault 0.0 ix m
              y = x + (T.int_to_float (Midi.byte_to_int d2 - cntr) * incr)
              z = if y < 0 then 0 else if y > 1 then 1 else y
          in (Map.insert ix z m, Just (ix0 + ix, z))
    _ -> (m, Nothing)

osc_cc_rel_f :: Osc_Cc_Opt -> Osc.OscSocket -> MVar Osc_Cc_St -> Pm.Proc_F
osc_cc_rel_f opt fd mv dat =
  case dat of
    Left e -> do
      r <- modifyMVar mv (\m -> return (osc_cc_st_edit opt e m))
      case r of
        Just (ix, z) -> Osc.sendMessage fd (Osc.message "/c_set" [Osc.int32 ix, Osc.Float z])
        Nothing -> return ()
    Right x -> print ("osc_cc_rel_f", x)

-- > osc_cc_rel (Osc.Tcp,"127.0.0.1",57110) (1,True) (16,16,11000,0x40,0.001)
osc_cc_rel :: Osc.OscSocketAddress -> (Double, Bool) -> Osc_Cc_Opt -> IO ()
osc_cc_rel address (delay, cc_read) opt = do
  print (address, delay, cc_read, opt)
  st <- if cc_read then osc_cc_init_map address opt else return Map.empty
  mv <- newMVar st
  fd <- Osc.openOscSocket address
  Pm.pm_midi_processor delay (osc_cc_rel_f opt fd mv)

-- * Osc ; Mpe ; Event

{- | (Ch,Ty,Cc) -> (D1,D2)

Mpe sends global messages on channel 0 (or alternately channel 15).
Mpe sends voice messages per-channel (velocity, pitch bend, pressure, brightness, release velocity).
-}
type Osc_Mpe_St = Map.Map (Midi.Byte, Midi.Byte, Midi.Byte) (Midi.Byte, Midi.Byte)

-- | Store incoming midi message at state.
osc_mpe_st_edit :: Midi.Cvm3 Midi.Byte -> Osc_Mpe_St -> Osc_Mpe_St
osc_mpe_st_edit (st, d1, d2) =
  let (ch, ty) = Midi.status_ch_ty st
      is_note_off = (==) 0x08
      is_cc = (==) 0x0B
  in Map.insert
      (ch, if is_note_off ty then 0x09 else ty, if is_cc ty then d1 else 0)
      (d1, if is_note_off ty then 0 else d2)

{- | c0 = initial control bus (13000), c_incr = control bus increment (10), ch = channel
Note that MPE arrives on channels 2-16 (ie. 1-15) and not on channel 1 (ie. 0).
-}
osc_mpe_ch :: (Int, Int) -> Midi.Byte -> Osc_Mpe_St -> Osc.Message
osc_mpe_ch (c0, c_incr) ch m =
  let u7_to_f i = fromIntegral i / 127.0
      u14_to_f i = fromIntegral i / 16383.0
      (x, w) = Map.findWithDefault (0, 0) (ch, 0x9, 0) m
      (_, z1) = Map.findWithDefault (0, 0) (ch, 0xA, 0) m -- polyphonic key pressure (non-standard) = z
      (_, y) = Map.findWithDefault (0, 0) (ch, 0xB, 74) m -- cc74 = 0x4A = brightness = y
      (z2, _) = Map.findWithDefault (0, 0) (ch, 0xD, 0) m -- channel key pressure (mpe standard) = z
      (dt1, dt2) = Map.findWithDefault (0, 0) (ch, 0xE, 0) m -- pitch bend = delta-x
      dt = u14_to_f (Common.bits_7_join_le (dt1, dt2)) - 0.5
      ix = fromIntegral c0 + (fromIntegral ch * fromIntegral c_incr)
      p = (fromIntegral x + (dt * 2.0)) / 127.0 -- (0, 1)
      param =
        [ if w == 0 then 0 else 1 -- 0 | 1
        , p
        , u7_to_f y
        , u7_to_f (z1 + z2)
        , 0.5
        , 0.5
        , 0.5
        , p
        ]
      n = genericLength param -- 8
  in Osc.message "/c_setn" (Osc.Int32 ix : Osc.Int32 n : map Osc.Float param)

osc_mpe_event_f :: Osc.OscSocket -> (Int, Int) -> MVar Osc_Mpe_St -> Pm.Proc_F
osc_mpe_event_f fd opt mv dat =
  case dat of
    Left e -> do
      modifyMVar_ mv (\m -> return (osc_mpe_st_edit e m))
      m <- readMVar mv
      let (st, _, _) = e
      Osc.sendMessage fd (osc_mpe_ch opt (Midi.status_ch st) m)
    Right x -> print ("osc_mpe_event_f", x)

osc_mpe_event :: Osc.OscSocketAddress -> Double -> (Int, Int) -> IO ()
osc_mpe_event address delay opt = do
  mv <- newMVar Map.empty
  fd <- Osc.openOscSocket address
  Pm.pm_midi_processor delay (osc_mpe_event_f fd opt mv)

-- * Osc ; Std ; Event (X=Mnn,Y=Mod,Z=Vel * Vol|Expr)

{-
ContinuousEvent output from a standard midi controller input:
- note-on allocates a voice from the end of the un-allocated voice queue
- a map of allocated voices is stored
- control messages (pitch bend, pressure, brightness) are sent to all allocated channels
- when a note-off arrives the voice is de-allocated and returned to the free-list

import qualified Deque.Strict as Deque {- deque -}

type Voice = Midi.Channel

-- | Free voices
type Voice_Free = Deque.Deque Voice

voice_free_init :: Voice_Free
voice_free_init = Deque.fromConsAndSnocLists [] [1..15]

-- | Get element at end of queue.
--
-- > voice_free_get voice_free_init -- 1
voice_free_get :: Voice_Free -> Maybe (Voice,Voice_Free)
voice_free_get = Deque.unsnoc

-- | Add element to front of queue.
--
-- > voice_free_put 16 voice_free_init
voice_free_put :: Voice -> Voice_Free -> Voice_Free
voice_free_put = Deque.cons

-- | Allocated notes: (mnn,channel) -> voice
type Voice_Alloc = Map.Map (Midi.Byte,Midi.Channel) Voice

voice_alloc_univ :: Voice_Alloc -> [Voice]
voice_alloc_univ = Map.elems

voice_alloc_add :: (Midi.Byte,Midi.Channel) -> Voice -> Voice_Alloc -> Voice_Alloc
voice_alloc_add = Map.insert

voice_alloc_get :: (Midi.Byte,Midi.Channel) -> Voice_Alloc -> Maybe Voice
voice_alloc_get = Map.lookup

voice_alloc_delete :: (Midi.Byte,Midi.Channel) -> Voice_Alloc -> Voice_Alloc
voice_alloc_delete = Map.delete

-- | (voice-set,osc-mpe-st)
type Osc_Std_St = (Voice_Free,Voice_Alloc)

osc_std_alloc_voice :: (Midi.Byte,Midi.Channel) -> Osc_Std_St -> (Maybe Voice,Osc_Std_St)
osc_std_alloc_voice k (fr,al) =
  case voice_free_get fr of
    Nothing -> (Nothing,(fr,al))
    Just (vc,fr') -> (Just vc,(fr',voice_alloc_add k vc al))

osc_std_free_voice :: (Midi.Byte,Midi.Channel) -> Osc_Std_St -> Osc_Std_St
osc_std_free_voice k (fr,al) =
  case voice_alloc_get k al of
    Nothing -> (fr,al) -- error
    Just vc -> (voice_free_put vc fr,voice_alloc_delete k al)

osc_std_st_edit :: Midi.Cvm3 Midi.Byte -> Osc_Std_St -> Osc_Std_St
osc_std_st_edit (st,d1,d2) o =
  let (ch,ty) = Midi.status_ch_ty st
  in case (ty,d2) of
       (0x09,0) -> osc_std_free_voice (d1,ch) o
       (0x09,_) -> snd (osc_std_alloc_voice (d1,ch) o)
       (0x08,_) -> osc_std_free_voice (d1,ch) o
       _ -> o
-}

-- * Print ; Plain

print_plain_f :: Bool -> Pm.Proc_F
print_plain_f hex m =
  case m of
    Left (st, d1, d2) -> putStrLn (Pp.byte_seq_pp hex [st, d1, d2])
    Right x ->
      let (w0, w1, w2, w3) = Pm.pm_msg_decode x
      in putStrLn (Pp.byte_seq_pp hex [w0, w1, w2, w3])

-- > print_plain True
print_plain :: Bool -> IO ()
print_plain = Pm.pm_midi_processor 1 . print_plain_f

-- * Print ; Csv ; Mnd

print_csv_mnd_f :: Osc.Time -> Pm.Proc_F
print_csv_mnd_f t0 m = do
  let pp (st, d1, d2) = do
        t <- Osc.time
        maybe (return ()) putStrLn (Csv.cvm_to_csv_mnd (t - t0) (Midi.cvm3_to_cvm (st, d1, d2)))
   in either pp (\_ -> return ()) m

print_csv_mnd :: IO ()
print_csv_mnd = Osc.time >>= \t0 -> Pm.pm_midi_processor 1 (print_csv_mnd_f t0)

-- * Stat

stat :: IO ()
stat = Pm.pm_device_tbl >>= \(h, d) -> putStrLn (unlines (T.table_pp T.table_opt_simple (h : d)))

-- * Sysex ; Send

sysex_send :: Double -> Int -> IO ()
sysex_send ms dst = do
  b <- fmap (map fromIntegral . B.unpack) B.getContents
  let sy_seq = Common.segment_at 0xF0 b
      wr fd dat = case b of
        0xF0 : _ -> Pm.pm_sysex_write fd dat >> Osc.pauseThread (ms * 0.001)
        _ -> error "sysex_send?"
  Pm.pm_with_output_device dst (\fd -> mapM_ (wr fd) sy_seq)

-- * Main

opt_def :: [T.OptUsr]
opt_def =
  [ ("cc-addr", "11000", "int", "c_set index zero for cc data")
  , ("cc-channels", "16", "int", "number of channels of cc-data sent")
  , ("cc-center", "64", "int", "center index for relative cc data")
  , ("cc-controls", "16", "int", "number of controls of cc-data sent")
  , ("cc-increment", "0.001", "real", "step increment for relative cc-data")
  , ("cc-read", "True", "bool", "read initial cc values by sending a c_getn message")
  , ("delay", "1", "real", "delay interval (milliseconds)")
  , ("device", "0", "int", "port midi device identifier (see status)")
  , ("event-addr", "13000", "int", "c_set index zero for event data")
  , ("event-incr", "10", "int", "c_set index increment for event data")
  , ("protocol", "Tcp", "string", "protocol (Tcp or Udp)")
  , ("hostname", "127.0.0.1", "string", "hostname")
  , ("port", "57110", "int", "port")
  ]

help :: [String]
help =
  [ "midi-osc-pm cmd opt"
  , ""
  , " osc cc rel [--cc-addr --cc-channels -cc-center --cc-controls --cc-incr --cc-read --delay --host --port]"
  , " osc mpe event [--delay --event-addr --event-incr --hostname --port]"
  , " print csv-mnd i ; i = integer"
  , " print plain {d|x} ; d = decimal, x = hexadecimal"
  , " stat"
  , " sysex send [--delay --device]"
  ]

main :: IO ()
main = do
  (o, a) <- T.opt_get_arg True help opt_def
  let cc_opt =
        ( T.opt_read o "cc-channels"
        , T.opt_read o "cc-controls"
        , T.opt_read o "cc-addr"
        , T.opt_read o "cc-center"
        , T.opt_read o "cc-increment"
        )
      cc_read = T.opt_read o "cc-read"
      delay = T.opt_read o "delay"
      device = T.opt_read o "device"
      event_addr = T.opt_read o "event-addr"
      event_incr = T.opt_read o "event-incr"
      protocol = T.opt_read o "protocol"
      hostname = T.opt_get o "hostname"
      port = T.opt_read o "port"
      address = (protocol, hostname, port)
  case a of
    ["osc", "cc", "rel"] -> osc_cc_rel address (delay, cc_read) cc_opt
    ["osc", "mpe", "event"] -> osc_mpe_event address delay (event_addr, event_incr)
    ["print", "plain", ty] -> print_plain (ty == "x")
    ["print", "csv-mnd", "i"] -> print_csv_mnd
    ["stat"] -> stat
    ["sysex", "send"] -> sysex_send delay device
    _ -> T.opt_usage help opt_def
