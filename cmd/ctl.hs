import Control.Monad {- base -}
import Data.IORef {- base -}
import Data.List {- base -}
import Data.Word {- base -}
import System.FilePath {- base -}
import Text.Printf {- base -}

import qualified Data.ByteString as B {- bytestring -}
import qualified Data.ByteString.Lazy as L {- bytestring -}

import qualified Music.Theory.Array.Csv as T {- hmt-base -}
import qualified Music.Theory.Array.Text as T {- hmt-base -}
import qualified Music.Theory.Byte as Byte {- hmt-base -}
import qualified Music.Theory.Math.Convert as T {- hmt-base -}
import qualified Music.Theory.Monad as T {- hmt-base -}
import qualified Music.Theory.Opt as T {- hmt-base -}
import qualified Music.Theory.Show as T {- hmt-base -}

import qualified Music.Theory.Array.Csv.Midi.Mnd as T {- hmt -}
import qualified Music.Theory.Time.Seq as T {- hmt -}

import Sound.Osc {- hosc -}

import Sound.Sc3.Server.Nrt {- hsc3 -}

import qualified Sound.Midi.Csv as M {- midi-osc -}
import qualified Sound.Midi.Osc as M {- midi-osc -}
import qualified Sound.Midi.Osc.Server as Server {- midi-osc -}
import qualified Sound.Midi.Pp as M {- midi-osc -}
import qualified Sound.Midi.Tuning as M {- midi-osc -}
import qualified Sound.Midi.Type as M {- midi-osc -}

-- * From-Csv-Mnd

-- (destination,print,transposition,gain,rw)
type Opt = (Int, Bool, Int, Double, Bool)

opt_rw :: Opt -> Bool
opt_rw (_, _, _, _, rw) = rw

vel_mul :: Int -> Double -> Int
vel_mul p q = round (fromIntegral p * q)

send_plain :: Transport m => Double -> Opt -> T.Mnd Double Int -> m ()
send_plain t0 (dst, prt, trs, gn, _) mnd = do
  let (tm, typ, d1, d2, ch, _) = mnd
      msg = case typ of
        "on" -> M.Note_On ch (d1 + trs) (vel_mul d2 gn)
        "off" -> M.Note_Off ch (d1 + trs) (vel_mul d2 gn)
        _ -> error "send_mnd"
  liftIO (pauseThreadUntil (t0 + tm))
  sendMessage (M.cvm_to_osc dst msg)
  when prt (liftIO (print mnd))

-- > let c_fn = "/home/rohan/sw/hmt/data/csv/mnd/all-notes-off.csv"
-- > mnd_plain (1,True) c_fn
mnd_plain :: Opt -> FilePath -> IO ()
mnd_plain opt fn = do
  dat <- T.csv_mnd_read fn
  t0 <- time
  Server.with_midi_osc Server.midi_osc_default_address [] (mapM_ (send_plain t0 opt) dat)

send_event :: Transport m => Double -> Opt -> (Time, T.Begin_End (T.Event Int)) -> m ()
send_event t0 (dst, prt, trs, gn, _) (tm, ev) = do
  let msg = case ev of
        T.Begin (d1, d2, ch, _) -> M.Note_On ch (d1 + trs) (vel_mul d2 gn)
        T.End (d1, d2, ch, _) -> M.Note_Off ch (d1 + trs) (vel_mul d2 gn)
  liftIO (pauseThreadUntil (t0 + tm))
  sendMessage (M.cvm_to_osc dst msg)
  when prt (liftIO (print (tm, ev, msg)))

-- > let c_fn = "/home/rohan/sw/hmt/data/csv/mnd/1080-C01.csv"
-- > mnd_event (1,True) c_fn
mnd_event :: Opt -> FilePath -> IO ()
mnd_event opt fn = do
  let rw = if opt_rw opt then T.wseq_remove_overlaps_rw T.event_eq_ol id else id
  w_sq <- fmap (T.csv_midi_parse_wseq_f (T.double_to_int round)) (T.load_csv fn)
  let t_sq = T.midi_wseq_to_midi_tseq (rw w_sq)
  t0 <- time
  Server.with_midi_osc Server.midi_osc_default_address [] (mapM_ (send_event t0 opt) t_sq)

-- * Mts

-- > nrt_csv_to_sysex "/home/rohan/mq.text" "/home/rohan/mq.syx"
nrt_csv_to_sysex :: FilePath -> FilePath -> IO ()
nrt_csv_to_sysex fn1 fn2 = do
  tbl <- T.csv_table_read_def id fn1
  let mk_freq e =
        case e of
          [_, mnn, cents] -> M.midi_detune_to_mts_frequency (read mnn, read cents)
          _ -> error "nrt_csv_to_sysex?"
      syx = M.mts_nrt_sysex (M.mts_all_devices, 0, M.mts_name (takeBaseName fn2), map mk_freq tbl)
  B.writeFile fn2 (B.pack (map fromIntegral syx))

-- > nrt_sysex_to_csv "/home/rohan/uc/the-center-is-between-us/trees/syx/mts/lmy.syx" "/dev/stdout"
nrt_sysex_to_csv :: FilePath -> FilePath -> IO ()
nrt_sysex_to_csv fn1 fn2 = do
  syx <- fmap (map fromIntegral . B.unpack) (B.readFile fn1)
  let (_, _, _, freq) = M.mts_nrt_sysex_decode syx
      pp k mts =
        let (mnn, cents) = M.mts_frequency_to_midi_detune mts
        in printf "%3d,%3d,%7.4f" k mnn cents
  writeFile fn2 (unlines (zipWith pp [0 :: Int ..] freq))

-- * Print

maybe_puts :: Maybe String -> IO ()
maybe_puts = maybe (return ()) putStrLn

recv_midi_plain :: Bool -> Message -> IO ()
recv_midi_plain hex = putStrLn . M.byte_seq_pp hex . M.encode_midi_message . M.osc_to_mm

-- > print_plain "x"
print_plain :: String -> IO ()
print_plain ty = Server.midi_osc_processor (recv_midi_plain (ty == "x")) (return ())

print_csv_mnd_f :: Time -> Message -> IO ()
print_csv_mnd_f t0 m = do
  t <- time
  maybe_puts (M.cvm_to_csv_mnd (t - t0) (M.osc_to_cvm m :: M.Channel_Voice_Message Int))

print_csv_mnd :: IO ()
print_csv_mnd = time >>= \t0 -> Server.midi_osc_processor (print_csv_mnd_f t0) (return ())

-- * Record

pad_left :: a -> Int -> [a] -> [a]
pad_left x n l = replicate (n - length l) x ++ l

pad_right :: a -> Int -> [a] -> [a]
pad_right x n l = l ++ replicate (n - length l) x

show_dec :: Show a => a -> String
show_dec = pad_left ' ' 3 . show

recv_midi :: IORef [(Time, Message)] -> Message -> IO ()
recv_midi st m = do
  t <- time
  case m of
    Message "/midi" _ -> modifyIORef st ((t, m) :)
    _ -> print ("?", m)

msg_to_bytes :: Message -> [Word8]
msg_to_bytes m =
  case m of
    Message "/midi" [Midi (MidiData _ i j k)] -> [i, j, k]
    Message "/midi" [Int32 _, Blob b] -> L.unpack b
    _ -> error "msg_to_bytes"

write_csv :: Int -> Double -> FilePath -> [(Double, Message)] -> IO ()
write_csv k t0 fn m =
  let f (tm, msg) =
        let tm' = T.double_pp k (tm - t0)
            msg' = map show_dec (msg_to_bytes msg)
        in tm' : msg'
      m' = map f m
      w = maximum (map length m')
      m'' = map (intercalate "," . pad_right "" w) m'
  in writeFile fn (unlines m'')

write_nrt :: Time -> FilePath -> [(Time, Message)] -> IO ()
write_nrt t0 fn m =
  let f (tm, msg) = bundle (tm - t0) [msg]
  in writeNrt fn (Nrt (map f m))

write_rec :: (Bool, Int) -> Double -> FilePath -> [(Double, Message)] -> IO ()
write_rec (csv, prec) = if csv then write_csv prec else write_nrt

record :: (Bool, Int) -> FilePath -> IO ()
record (csv, prec) fn = do
  t0 <- time
  st <- newIORef []
  let end_f = readIORef st >>= write_rec (csv, prec) t0 fn . reverse
  Server.midi_osc_processor (recv_midi st) end_f

-- * Reset

-- reset_f :: Connection Fd.Udp.Udp ()
reset_f :: Connection OscSocket ()
reset_f = do
  sendMessage (Message "/reset" [])
  _ <- waitReply "/reset.reply"
  return ()

p_to_a :: OscPort -> OscSocketAddress
p_to_a p = (Udp, "127.0.0.1", p)

reset :: Int -> IO ()
reset p = Server.with_midi_osc (p_to_a p) [Server.receive_all] reset_f

-- * Send

type Reader = String -> [Word8]

{- | Read Hs literals

>>> hs_read "[0x90,69,127]" == [0x90,0x45,0x7F]
True
-}
hs_read :: Reader
hs_read = read

{- | Read hex literals

>>> hex_read "90457F" == [0x90,0x45,0x7F]
True
-}
hex_read :: Reader
hex_read = Byte.read_hex_byte_seq

parse_reader :: String -> Reader
parse_reader nm =
  case nm of
    "hs" -> hs_read
    "hex" -> hex_read
    _ -> error ("parse_reader: " ++ nm)

send :: Transport m => Int -> Blob -> m ()
send dst dat = do
  let m = Message "/midi" [int32 dst, Blob dat]
  liftIO (print (M.osc_to_mm m :: M.Midi_Message Int))
  sendMessage m

read_txt_and_send :: Transport m => Reader -> Int -> m ()
read_txt_and_send rd dst =
  T.repeatM_
    ( do
        s <- liftIO getLine
        send dst (L.pack (rd s))
    )

read_bin_and_send :: Transport m => Int -> m ()
read_bin_and_send dst = do
  b <- liftIO L.getContents
  send dst b

-- * Status

status_pp :: Server.Status -> IO ()
status_pp (_, src_nm, _, dst_nm) = do
  let hdr = ["Type", "Index", "Name", "Port"]
  putStrLn (unlines (T.table_pp T.table_opt_simple (hdr : (src_nm ++ dst_nm))))

status :: Int -> IO ()
status p = Server.with_midi_osc (p_to_a p) [Server.receive_all] (Server.status_read >>= fmap liftIO status_pp)

-- * Main

opt_def :: [T.OptUsr]
opt_def =
  [ ("destination", "-1", "int", "midi destination, -1 sends to other midi-osc clients")
  , ("port", "57150", "int", "midi-osc-daemon udp port")
  , ("print", "True", "bool", "print outgoing messages")
  , ("transpose", "0", "int", "transposition (semitones)")
  , ("gain", "1", "real", "gain (linear multiplier)")
  , ("rewrite", "False", "bool", "re-write input (event) to not contain overlaps")
  ]

help :: [String]
help =
  [ "midi-osc-ctl"
  , ""
  , "  from-csv-mnd plain|event [opt] csv-file"
  , "  mts nrt csv-to-sysex csv-file syx-file"
  , "  mts nrt sysex-to-csv syx-file csv-file"
  , "  print csv-mnd i ; i = integer"
  , "  print plain {d|x} ; d = decimal, x = hexadecimal"
  , "  record csv precision:int csv-file"
  , "  record nrt nrt-file"
  , "  reset [--port=udp-port:int]"
  , "  send [--port=udp-port:int {bin | hex | hs} destination:int"
  , "  status [--port=udp-port:int]"
  ]

main :: IO ()
main = do
  (o, a) <- T.opt_get_arg True help opt_def
  let dst = T.opt_read o "destination"
      gn = T.opt_read o "gain"
      prt = T.opt_read o "print"
      trs = T.opt_read o "transpose"
      rw = T.opt_read o "rewrite"
      p = T.opt_read o "port"
  case a of
    ["from-csv-mnd", "plain", fn] -> mnd_plain (dst, prt, trs, gn, undefined) fn
    ["from-csv-mnd", "event", fn] -> mnd_event (dst, prt, trs, gn, rw) fn
    ["mts", "nrt", "csv-to-sysex", fn1, fn2] -> nrt_csv_to_sysex fn1 fn2
    ["mts", "nrt", "sysex-to-csv", fn1, fn2] -> nrt_sysex_to_csv fn1 fn2
    ["print", "csv-mnd", "i"] -> print_csv_mnd
    ["print", "plain", ty] -> print_plain ty
    ["record", "csv", prec, fn] -> record (True, read prec) fn
    ["record", "nrt", fn] -> record (False, -1) fn
    ["reset"] -> reset p
    ["send", "bin", d] -> Server.with_midi_osc (p_to_a p) [] (read_bin_and_send (read d))
    ["send", r, d] -> Server.with_midi_osc (p_to_a p) [] (read_txt_and_send (parse_reader r) (read d))
    ["status"] -> status p
    _ -> T.opt_usage help opt_def
