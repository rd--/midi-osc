import System.Environment {- base -}

import Data.Word {- base -}

import qualified Data.Vector.Storable as V {- vector -}

import qualified Sound.Osc as Osc {- hosc -}

import qualified Sound.Midi.Csv as Csv {- midi-osc -}
import qualified Sound.Midi.Rt as Rt {- midi-osc -}
import qualified Sound.Midi.Type as M {- midi-osc -}

-- * PRINT ; CSV ; MND

print_csv_mnd_msg :: Osc.Time -> Osc.Time -> V.Vector Word8 -> IO ()
print_csv_mnd_msg t0 _dt msg = do
  t <- Osc.time
  let maybe_print = maybe (return ()) putStrLn
  case map fromIntegral (V.toList msg) of
    [st, d1, d2] -> maybe_print (Csv.cvm_to_csv_mnd (t - t0) (M.cvm3_to_cvm (st, d1, d2)))
    _ -> return ()

print_csv_mnd :: Int -> IO ()
print_csv_mnd k = Osc.time >>= \t0 -> Rt.rtm_process_default_input k (print_csv_mnd_msg t0)

-- * MAIN

help :: [String]
help =
  [ "print csv-mnd {i} port-number:int ; i = integer"
  , "print plain {d|x} port-number:int ; d = decimal, x = hexadecimal"
  , "status {default-inputs | default-outputs | report}"
  , "sysex send destination:int"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["print", "csv-mnd", "i", ix] -> print_csv_mnd (read ix)
    ["print", "plain", ty, ix] -> Rt.rtm_print_plain (read ix) (ty == "x")
    ["status", "default-inputs"] -> Rt.rtm_status_default_input_ports
    ["status", "default-outputs"] -> Rt.rtm_status_default_output_ports
    ["status", "report"] -> Rt.rtm_status_report
    _ -> putStrLn (unlines help)
