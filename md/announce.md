<https://lists.linuxaudio.org/archives/linux-audio-announce/2004-December/000501.html>

~~~~
To: linux-audio-announce@music.columbia.edu
From: Rohan Drape <rd@alphalink.com.au>
Date: 15 Dec 2004 01:23:17 +1100
Subject: [Announce] midi.osc - MIDI Packet Publication Daemon

midi.osc publishes MIDI packets on the local host MIDI system as OSC
packets over a UDP connection.  midi.osc allows any environment that
supports OSC to act as a MIDI client, sending and receiving MIDI data,
and monitoring and initiating changes to the host MIDI system.

There is support for ALSA Sequencer (Linux) and CoreMidi (OSX) hosts.
There are some issues with the ALSA send support, any help on this
would be appreciated.

A source archive and documentation are available at:
<http://www.alphalink.com.au/~rd/>

Regards,
Rohan
~~~~
