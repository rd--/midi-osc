# midi-osc-ctl

## from-csv-mnd

Load CSV `MND` or `MNDD` file and send scheduled MIDI data to the
specified destination port at `midi-osc-daemon`.

Input CSV files may indicate fractional note and velocity values.

`midi-osc` has a message type for fractional values, but here
fractional inputs are rounded before sending.

In the `plain` variant data must be MND, and is sent directly.

In the `event` variant data may be MND or MNDD and is parsed into an event sequence before sending.
This process discards mismatched on/off elements, so cannot be used to send an all-notes-off file.

## mts

mts = midi tuning standard

This command is not connected to midi-osc.

It converts between MTS and CSV.

~~~~
$ hmt-scala midi-table mnn-cents d12 meanquar 0 0 > /tmp/mq.csv
$ midi-osc-ctl mts nrt csv-to-sysex /tmp/mq.csv /tmp/mq.syx
$ midi-osc-ctl mts nrt sysex-to-csv /tmp/mq.syx /dev/stdout
  0, 0.0000
  0,76.0437
  1,93.1519
  3,10.2600
  3,86.3098
  5, 3.4180
  5,79.4678
  6,96.5759
  7,72.6257
  8,89.7339
 10, 6.8420
 10,82.8918
...
$
~~~~

## print

A printer for messages passing across `midi-osc-daemon`.

With `-x` prints in hexadecimal and is equivalent to `amidi -d`.

## record

Monitor messages passing across `midi-osc-daemon` until a user interrupt
signal (Ctl-c) is received then write either an `NRT` or `CSV` file of
the midi data received.

## reset

Send `/reset` message to `midi-osc-daemon`.

## send

Read midi commands written as plain text lists of unsigned 8-bit
integers (ie. `[0x90,0x40,0xff]` or `[176,0,24]` etc.) at `stdin` and
send them to `midi-osc`.

~~~~
$ echo "[176,123,0]" | midi-osc-ctl send -d 1
$ echo "[0xF0,0x43,0x10,0x01,0x06,0x18,0xF7]" | midi-osc-ctl send
~~~~

## status

Pretty printer (TEXT) for `/status.reply` and `/name.reply` messages.

~~~~
$ midi-osc-ctl status
       TYPE INDEX         NAME                PORT
----------- ----- ------------ -------------------
     source     0       System               Timer
     source     1       System            Announce
     source     2 Midi Through Midi Through Port-0
     source     3        UA-25        UA-25 MIDI 1
     source     4    jack_midi                port
     source     5     midi-osc                 Out
destination     0       System               Timer
destination     1 Midi Through Midi Through Port-0
destination     2        UA-25        UA-25 MIDI 1
destination     3    jack_midi                port
destination     4     midi-osc                  In
----------- ----- ------------ -------------------
$
~~~~
