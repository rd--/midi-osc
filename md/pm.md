# midi-osc-pm

PM=[PORTMIDI](https://hackage.haskell.org/package/PortMidi).

## print

Print incoming MIDI messages.

~~~~
$ midi-osc-pm print plain x
90 3E 1B
90 41 0B
90 41 00
90 3E 00
$ midi-osc-pm print csv-mnd i
4.6200,program-change,15,0,0
7.9560,on,75,10,0
9.4369,off,75,0,0
$
~~~~

## stat

Print available input and output port names.

~~~~
$ midi-osc-pm stat
ID INTERFACE                NAME INPUT OUTPUT  OPEN
-- --------- ------------------- ----- ------ -----
 0      ALSA Midi Through Port-0 False   True False
 1      ALSA Midi Through Port-0  True  False False
 2      ALSA        UA-25 MIDI 1 False   True False
 3      ALSA        UA-25 MIDI 1  True  False False
-- --------- ------------------- ----- ------ -----
$
~~~~

## sysex

`sysex send` reads SYSEX data from `stdin` and sends it to the
indicated destination, inserting a delay of the indicated duration
(given in milliseconds) between each SYSEX packet.
