# midi-osc-sc3

A configurable server for listening to control-change (cc) messages at `midi-osc` and
translating them into commands for the supercollider synthesiser
[scsynth](http://audiosynth.com/).

Sound.SC3.Lang.Control.Midi.OSC.[SC3](?t=hsc3-lang&e=Sound/SC3/Lang/Control/Midi/OSC/SC3.hs).

The command set is:

------ -------------------------------------------------------
/clear
/param cc:int k:int p:string l:float r:float w:string u:string
/synth k:int nm:str
------ -------------------------------------------------------

- cc = midi controller number (zero-indexed)
- k = node/group-id
- p = parameter name
- (l,r) = left (min) and right (max) values of range to map (0-127) midi data to
- w = warp name (lin, exp, sin, cos, amp, db)
- u = unit (for printing)

[[hs](?t=midi-osc&e=cmd/sc3.hs)]
