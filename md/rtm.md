# midi-osc-rtm

rtm=[RtMidi](https://hackage.haskell.org/package/RtMidi).

## print

Print incoming MIDI messages arriving at indicated port.

`plain x` (x=hexadecimal) prints using the same format as `amidi -d`.

~~~~
$ midi-osc-rtm print plain x 1
90 3E 1B
90 41 0B
90 41 00
90 3E 00
$ midi-osc-rtm print csv-mnd i 1
4.6200,program-change,15,,0
7.9560,on,75,10,0
9.4369,off,75,0,0
$
~~~~

## status

Print available input and output port indices and names.

`status report` prints the port indices required by commands that connect to input or output ports.

~~~~
$ midi-osc-rtm status report
API = alsa
 Inputs:
  0 = Midi Through:Midi Through Port-0 14:0
  1 = UA-25:UA-25 MIDI 1 20:0
 Outputs:
  0 = Midi Through:Midi Through Port-0 14:0
  1 = UA-25:UA-25 MIDI 1 20:0
$
~~~~
