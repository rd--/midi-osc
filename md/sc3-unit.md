# midi-osc-sc3-unit

Read midi data and write (0,1) values onto control buses or into buffers at [scsynth](http://audiosynth.com/).

Read modes are 'ctl' to process CC messages, and 'mnn' to process note messages.

Write modes are 'ctl' to write to control buses and 'buf' to write to the indicated buffer.
