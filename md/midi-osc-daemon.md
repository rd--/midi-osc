MIDI-OSC-DAEMON(1)
==================

NAME
----
midi-osc-daemon - MIDI Packet Publication Daemon

SYNOPSIS
--------
midi-osc-daemon [options]

OPTIONS
-------
*-p*
:   Set the port number (default=57150).

DESCRIPTION
-----------
midi-osc-daemon is a transport daemon that allows any environment that
supports OSC to act as a MIDI client, sending and receiving MIDI data,
and monitoring and initiating changes to the host MIDI system.

midi-osc-daemon supports ALSA and CoreMidi hosts.

Clients register with the midi-osc-daemon server by sending a "request
notification" packet of the form:

    /receive category:<int>

The category is a bit-mask indicating the classes of
packets to receive. The bit locations are:

    #define   RECEIVE_MIDI   0x00000001
    #define   RECEIVE_META   0x00000002
    #define   RECEIVE_ALL    0xFFFFFFFF

Clients can be registered at an address that is not that of the packet
that sends the request by sending a "request notification at" packet:

    /receive_at category:<int> port-number:<int> host-name:<string>

where category is as for `/receive` and where port-number and host-name
give the address of the client to be registered.

Once a client is registered subsequent `/receive` and `/receive_at`
messages edit the category value for that client.  To delete the
client from the register send a request with a category value of
`-1`.

After requesting notification the client will receive all relevant
MIDI packets sent over the host MIDI system.

The MIDI packets sent by midi-osc-daemon can take three forms:

    /midi message:<midi>
    /midi source-identifier:<int> packet-data:<blob>
    /fmidi3 status-byte:<int> data1:<float> data2:<float>

The `/fmidi3` form allowing fractional midi note numbers and greater
precision for velocity and contoller values.

The change notification packets sent by midi-osc-daemon are of the form:

    /change message-identifier:<int>

Clients can request a server reset by sending a `/reset` message
which requires no argument.  The requesting client receives a
`/reset.reply` and all clients receive a `/change` packet.

Clients request to receive a MIDI system status packet by sending a
"request status" message, `/status`, to the midi-osc-daemon server.  This
message has no arguments.  The server replies with a `/status.reply`
of the form:

    /status.reply number-of-sources:<int> number-of-destinations:<int>

The MIDI ports at the host system are given and addressed as
enumerated sets of source and destination ports.  Source and
destination ports are each addressed using zero indexed <int>
values.

Clients can query the name of a MIDI port by sending a "request name"
message of the form:

    /name direction:<string> port-identifier:<int>

The direction should be "source" or "destination".  The server replies with:

    /name.reply device-name:<string> port-name:<string>

To send midi a message is sent to midi-osc-daemon of the form:

    /midi message:<midi>
    /midi port-id:<int> message:<blob>
    /fmidi3 status:<int> data1:<float> data2:<float>

If port-id is 255 (or -1) the message will not be sent to an actual midi port
but directed to clients of midi-osc. `/fmidi3` messages are only ever
sent to other clients.

midi-osc-daemon implements only a subset of the OSC protocol.  In particular
it does not implement the pattern matching rules and does not implement
a scheduler for incoming messages.

midi-osc-daemon drops all unrecognized incoming packets.

AUTHOR
------
Rohan Drape <http://rohandrape.net/>.

SEE ALSO
--------
OSC(7) <http://cnmat.berkeley.edu/OSC/>
