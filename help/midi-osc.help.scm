(import (rhs) (sosc) (rsc3))

;; Create a UDP socket.
(define m (udp:open "127.0.0.1" 57150))

;; Request to receive MIDI packets.
(send m (message "/receive" (list #xffff)))

;; Request status information.
(define read-status
  (lambda (m)
    (send m (message "/status" (list)))
    (cdr (wait m "/status.reply"))))

(define status-i car)
(define status-o cadr)

;; Collect I/O names
(define collect-io
  (lambda (m)
    (let* ((st (read-status m))
           (f (lambda (ty)
                (lambda (i)
                  (send m (message "/name" (list ty i)))
                  (cdr (wait m "/name.reply"))))))
      (list (map (f "source") (enum-from-to 0 (status-i st)))
            (map (f "destination") (enum-from-to 0 (status-o st)))))))

(collect-io m)

;; Request the nth index source name.
(define n 3)

(send m (message "/name" (list "source" n)))
(wait m "/name.reply")

;; Request the nth index destination name.
(send m (message "/name" (list "destination" n)))
(wait m "/name.reply")

;; Request reset.
(send m (message "/reset" (list)))
(wait m "/reset.reply")

;; Send midi data.
(let ((p (u8-list->bytevector (list #x90 64 32))))
  (send m (message "/midi" (list n p))))

;; A loop to print incoming packets.
(letrec ((f (lambda ()
	      (display (recv m))
	      (newline)
	      (f))))
  (f))

;; Unregister address.
(send m (message "/receive" (list -1)))

;; Test structure of /midi packet.
(let* ((p (u8-list->bytevector (list 128 64 32)))
       (m (encode-osc (message "/midi" (list n p)))))
  (decode-osc m))
