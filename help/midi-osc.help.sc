// Create a <NetAddr> object and register a printer procedure
// to receive incoming packets.
(
a = NetAddr.new("127.0.0.1", 57150);
f = { arg time, responder, message ;
	[time, message].postln
	} ;
OSCresponder.new(a, '/midi', f).add;
OSCresponder.new(a, '/change', f).add;
OSCresponder.new(a, '/reset.reply', f).add;
OSCresponder.new(a, '/name.reply', f).add;
OSCresponder.new(a, '/status.reply', f).add;
)

// Request to receive messages from the midi-osc server.
a.sendMsg("/receive", 0xFFFF);

// Request status information.
a.sendMsg("/status");

// Request the nth index source and destination names.
(
n = 3;
a.sendMsg("/name", "source", n);
a.sendMsg("/name", "destination", n);
)

// Request reset.
a.sendMsg("/reset");

// Unregister address.
a.sendMsg("/receive", -1);
