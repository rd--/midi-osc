-- midi-osc-sc3-unit
--
-- midi controls are mapped to unit (0,1) sc3 control buses

import qualified Data.ByteString.Lazy as B {- bytestring -}

import Sound.OSC {- hosc -}
import qualified Sound.OSC.FD as FD {- hosc -}

import Sound.SC3 {- hsc3 -}

instr :: UGen
instr =
    let f = lagIn 1 1 0.1 * 220 + 220
        a = lagIn 1 2 0.1 * 0.1
        l = lagIn 1 3 0.1 * 2 - 1
    in out 0 (pan2 (sinOsc AR f 0) l a)

main :: IO ()
main = withSC3 (sendMessage (c_setn [(1,[0.5,0.5,0.5])]) >> play instr)

-- play synthesisers...
send_note :: IO ()
send_note = do
  fd <- FD.openUDP "127.0.0.1" 57150
  FD.sendMessage fd (Message "/midi" [Int32 3,Blob (B.pack [0x90,64,32])])
  FD.pauseThread 1
  FD.sendMessage fd (Message "/midi" [Midi (MIDI 3 0x80 64 0)])
  FD.close fd
