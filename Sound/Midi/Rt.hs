-- | RTmidi <https://www.music.mcgill.ca/~gary/rtmidi/>
module Sound.Midi.Rt where

import Control.Concurrent {- base -}
import Control.Exception {- base -}
import Control.Monad {- base -}
import Data.Word {- base -}
import System.IO {- base -}

import qualified Data.Vector.Storable as Vector {- vector -}

import qualified Sound.RtMidi as R {- RtMidi -}
import qualified Sound.RtMidi.Report as R {- RtMidi -}

import qualified Sound.Osc as Osc {- hosc -}

import qualified Sound.Midi.Pp as Pp {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

rtm_default_queue_size :: Int
rtm_default_queue_size = 100

rtm_with_default_input :: (R.InputDevice -> IO t) -> IO t
rtm_with_default_input = bracket R.defaultInput R.closePort

rtm_with_input :: R.Api -> String -> Int -> (R.InputDevice -> IO t) -> IO t
rtm_with_input api client_name queue_size = do
  let open_input = R.createInput api client_name queue_size
  bracket open_input R.closePort

-- | RtMidi process function type
type Rtm_Process_F = (Double -> Vector.Vector Word8 -> IO ())

vector_to_cvm3 :: Vector.Vector Word8 -> Maybe (Midi.Cvm3 Word8)
vector_to_cvm3 v = if Vector.length v == 3 then Just (fromIntegral (v Vector.! 0), v Vector.! 1, v Vector.! 2) else Nothing

vector_to_cvm4 :: Vector.Vector Word8 -> Maybe (Midi.Cvm4 Word8)
vector_to_cvm4 = fmap Midi.cvm3_to_cvm4 . vector_to_cvm3

-- * Poll

rtm_thread_delay :: Osc.Time -> IO ()
rtm_thread_delay = Osc.pauseThread

rtm_process_input :: Int -> Rtm_Process_F -> R.InputDevice -> IO ()
rtm_process_input k f i = do
  R.openPort i k "midi-osc"
  R.ignoreTypes i False False False
  forever
    ( R.getMessageSized i 512
        >>= \(dt, msg) ->
          when (Vector.length msg > 0) (f dt msg)
            >> rtm_thread_delay 0.001
    )

rtm_process_default_input :: Int -> Rtm_Process_F -> IO ()
rtm_process_default_input k f = rtm_with_default_input (rtm_process_input k f)

-- * Print ; Plain

rtm_print_plain_msg :: Bool -> Double -> Vector.Vector Word8 -> IO ()
rtm_print_plain_msg hex _dt msg = putStrLn (Pp.byte_seq_pp hex (Vector.toList msg))

-- > rtm_print_plain 1 True
rtm_print_plain :: Int -> Bool -> IO ()
rtm_print_plain k hex = rtm_process_default_input k (rtm_print_plain_msg hex)

-- * Status

rtm_status_api_list :: IO [String]
rtm_status_api_list = do
  a <- R.compiledApis
  mapM R.apiName a

rtm_port_pp :: String -> (Int, String) -> String
rtm_port_pp txt (ix, nm) = txt ++ unwords [show ix, "=", nm]

rtm_api_report_pp :: R.ApiReport -> [String]
rtm_api_report_pp r =
  concat
    [
      [ unwords ["API", "=", R.apiRepName r]
      , " Inputs:"
      ]
    , map (rtm_port_pp "  ") (R.apiInPorts r)
    , [" Outputs:"]
    , map (rtm_port_pp "  ") (R.apiOutPorts r)
    ]

rtm_status_report :: IO ()
rtm_status_report = do
  r <- R.buildReport
  mapM_ putStrLn (concatMap rtm_api_report_pp (R.apiReports r))

rtm_status_default_input_ports :: IO ()
rtm_status_default_input_ports = do
  i <- R.defaultInput
  p <- R.listPorts i
  putStrLn (unlines ("Default Input:" : map (rtm_port_pp " ") p))

rtm_status_default_output_ports :: IO ()
rtm_status_default_output_ports = do
  i <- R.defaultOutput
  p <- R.listPorts i
  putStrLn (unlines ("Default Output:" : map (rtm_port_pp " ") p))

-- * Callback (segfault - debian 5.10.4 x64)

-- | Wait (pause thread) until char arrives at stdin.
rtm_wait_getc :: IO ()
rtm_wait_getc = do
  hSetBuffering stdin NoBuffering
  _ <- getChar
  return ()

-- | Wait (pause thread) forever.
rtm_wait_sleep :: IO ()
rtm_wait_sleep = do
  _ <- forever (threadDelay maxBound)
  return ()

-- | Run function /f/ at port /k/ of input /i/ until 'getChar'
rtm_process_input_getc :: Int -> Rtm_Process_F -> R.InputDevice -> IO ()
rtm_process_input_getc k f i = do
  R.setCallback i f
  R.openPort i k "midi-osc"
  R.ignoreTypes i False False False
  rtm_wait_getc

-- | 'rtm_with_default_input' of 'rtm_process_input_getc'
rtm_process_default_input_getc :: Int -> Rtm_Process_F -> IO ()
rtm_process_default_input_getc k f = rtm_with_default_input (rtm_process_input_getc k f)

{-

o <- R.defaultOutput
R.openPort o 1 "midi-osc"
R.sendMessage o (Vector.fromList [0xF0,0x00,0x20,0x6B,0x7F,0x42,0x01,0x00,0x01,0x20,0xF7])

-}
