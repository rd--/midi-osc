-- | Midi Pp
module Sound.Midi.Pp where

import Data.Char {- base -}
import Numeric {- base -}

import qualified Sound.Midi.Type as Midi {- midi-osc -}

pad_left :: a -> Int -> [a] -> [a]
pad_left x n l = replicate (n - length l) x ++ l

show_hex :: (Integral a, Show a) => Int -> a -> String
show_hex k n = pad_left '0' k (map toUpper (showHex n ""))

show_hex_0x :: (Integral t, Show t) => Int -> t -> String
show_hex_0x k = ("0x" ++) . show_hex k

show_dec :: Show t => t -> String
show_dec = pad_left ' ' 3 . show

-- | Pretty print byte sequence.
byte_seq_pp :: (Integral t, Show t) => Bool -> [t] -> String
byte_seq_pp hex = unwords . map (if hex then show_hex 2 else show_dec)

{- | Pretty printer for Cvm3, flag to print in 2-Char hex or 3-Char decimal.

> cvm3_pp True (0x90,0xB3,0x05) == "90 B3 05"
> cvm3_pp False (0x90,0xB3,0x05) == "144 179   5"
-}
cvm3_pp :: Bool -> Midi.Cvm3 Int -> String
cvm3_pp hex = byte_seq_pp hex . Midi.cvm3_to_list id

-- | Print to 4 decimal places.
time_stamp_pp :: Midi.Time_Stamp -> String
time_stamp_pp t = showFFloat (Just 4) t ""
