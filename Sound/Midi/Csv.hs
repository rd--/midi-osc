-- | Midi Csv
module Sound.Midi.Csv where

import Data.List {- base -}

import qualified Sound.Midi.Common as Common {- midi-osc -}
import qualified Sound.Midi.Pp as Pp {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

{- | Print STATUS in hex and DATA in decimal.

>>> cvm3_to_csv 12.3456 (0x90,69,127)
"12.3456,0x90,69,127"
-}
cvm3_to_csv :: Show t => Midi.Time_Stamp -> Midi.Cvm3 t -> String
cvm3_to_csv t (st, d1, d2) = intercalate "," [Pp.time_stamp_pp t, Pp.show_hex_0x 2 st, show d1, show d2]

{- | Print MSG-TYPE and CHANNEL in hex and DATA in decimal.

>>> cvm4_to_csv 12.3456 (0x9,0x0,69,127)
"12.3456,0x9,0x0,69,127"
-}
cvm4_to_csv :: Show t => Midi.Time_Stamp -> Midi.Cvm4 t -> String
cvm4_to_csv t (ty, ch, d1, d2) =
  intercalate "," [Pp.time_stamp_pp t, Pp.show_hex_0x 1 ty, Pp.show_hex_0x 1 ch, show d1, show d2]

{- | 'Channel_Voice_Message' to CSV/MND string.  Strings are:

on=note-on, off=note-off, cc=control-change, prg=program-change, aft=channel-aftertouch, pb=pitch-bend

>>> cvm_to_csv_mnd 12.3456 (Midi.Note_On 0 69 127)
Just "12.3456,on,69,127,0,"

>>> cvm_to_csv_mnd 12.3456 (Midi.Note_Off 0 (69.5::Double) 0)
Just "12.3456,off,69.5,0,0,"

>>> cvm_to_csv_mnd 12.3456 (Midi.Program_Change 0 0x64)
Just "12.3456,program-change,100,,0,"
-}
cvm_to_csv_mnd :: (Show t, Num t) => Midi.Time_Stamp -> Midi.Channel_Voice_Message t -> Maybe String
cvm_to_csv_mnd t m =
  let jn = Just . intercalate ","
  in case m of
      Midi.Note_On ch d1 d2 -> jn [Pp.time_stamp_pp t, "on", show d1, show d2, show ch, ""]
      Midi.Note_Off ch d1 _ -> jn [Pp.time_stamp_pp t, "off", show d1, "0", show ch, ""]
      Midi.Control_Change ch d1 d2 -> jn [Pp.time_stamp_pp t, "cc", show d1, show d2, show ch, ""]
      Midi.Program_Change ch d1 -> jn [Pp.time_stamp_pp t, "program-change", show d1, "", show ch, ""]
      Midi.Channel_Aftertouch ch d1 -> jn [Pp.time_stamp_pp t, "aft", show d1, "", show ch, ""]
      Midi.Pitch_Bend ch d1 d2 -> jn [Pp.time_stamp_pp t, "pb", show (Common.bits_7_join (d1, d2)), "", show ch, ""]
      _ -> Nothing
