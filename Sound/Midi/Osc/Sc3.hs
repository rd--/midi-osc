-- | midi-osc-sc3
module Sound.Midi.Osc.Sc3 where

import qualified Data.List.Split as Split {- split -}

import qualified Sound.Osc as Osc {- hsc3 -}

-- * Param (Cc -> node/control-name)

-- | Parameter meta-data.
data Param = Param
  { param_cc_id :: Int
  , param_node_id :: Int
  , param_name :: String
  , param_min :: Double
  , param_max :: Double
  , param_warp :: String
  , param_unit :: String
  }
  deriving (Read, Show)

-- | Type-specialised 'read'.
parse_param_read :: String -> Param
parse_param_read = read

-- | csv = comma separated values
parse_param_csv :: String -> Param
parse_param_csv s =
  case Split.splitOn "," s of
    [c, k, nm, l, r, w, u] -> param (read c, read k, nm, read l, read r, w, u)
    _ -> error "parse_param_csv"

-- | ws = white-space
parse_param_ws :: String -> Param
parse_param_ws s =
  case words s of
    [c, k, nm, l, r, w, u] -> param (read c, read k, nm, read l, read r, w, u)
    _ -> error "parse_param_ws"

param_to_datum :: Param -> [Osc.Datum]
param_to_datum cfg =
  let Param c k nm l r w u = cfg
  in [Osc.int64 c, Osc.int64 k, Osc.string nm, Osc.double l, Osc.double r, Osc.string w, Osc.string u]

param_to_message :: Param -> Osc.Message
param_to_message = Osc.message "/param" . param_to_datum

clear :: Osc.Message
clear = Osc.message "/clear" []

with_midi_osc_sc3 :: Osc.Connection Osc.OscSocket a -> IO a
with_midi_osc_sc3 =
  let address = (Osc.Udp, "127.0.0.1", 57300)
  in Osc.withTransport (Osc.openOscSocket address)

-- | Unpacked 'Param', ie. (controller-id,node/group-id,name,min-value,max-value,warp,unit).
type Param' = (Int, Int, String, Double, Double, String, String)

param :: Param' -> Param
param (cc, k, nm, l, r, w, u) = Param cc k nm l r w u
