-- | @midi-osc-daemon@ interaction.
module Sound.Midi.Osc.Server where

import Control.Exception {- base -}
import Control.Monad {- base -}
import Data.Bits {- base -}
import Data.Int {- base -}
import Data.Maybe {- base -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Osc.Fd {- hosc -}

import qualified Sound.Midi.Osc {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

-- * Io

-- | Default port.
midi_osc_default_address :: Osc.OscSocketAddress
midi_osc_default_address = (Osc.Udp, "127.0.0.1", 57150)

-- | The midi-osc connection, Udp.
type MidiOsc_Fd = Osc.OscSocket

-- | Default connection to midi-osc daemon.
midi_osc_fd :: IO MidiOsc_Fd
midi_osc_fd = Sound.Osc.Fd.openOscSocket midi_osc_default_address

-- | Run /f/ receiving indicated messages, or none.
with_midi_osc :: Osc.OscSocketAddress -> [Receive_Category] -> Osc.Connection MidiOsc_Fd a -> IO a
with_midi_osc address category f =
  let f' =
        if null category
          then f
          else do
            Osc.sendMessage (receive_msg category)
            received <- f
            Osc.sendMessage receive_nil_msg
            return received
  in Osc.withTransport (Osc.openOscSocket address) f'

-- | 'receive_all' at default port.
with_midi_osc_def :: Osc.Connection MidiOsc_Fd a -> IO a
with_midi_osc_def = with_midi_osc midi_osc_default_address [receive_all]

-- * Receive

type Receive_Category = Int32

receive_midi :: Receive_Category
receive_midi = 0x0000001

receive_meta :: Receive_Category
receive_meta = 0x0000002

receive_all :: Receive_Category
receive_all = 0xFFFFFFF

receive_nil :: Receive_Category
receive_nil = -0x0000001

receive_category_encode :: (Bits n, Num n) => [n] -> n
receive_category_encode = foldl (.|.) 0

-- * Osc.Messages

receive_msg :: [Receive_Category] -> Osc.Message
receive_msg c = Osc.Message "/receive" [Osc.Int32 (receive_category_encode c)]

receive_all_msg :: Osc.Message
receive_all_msg = receive_msg [receive_all]

receive_nil_msg :: Osc.Message
receive_nil_msg = receive_msg [receive_nil]

name_msg :: String -> Int32 -> Osc.Message
name_msg ty n = Osc.Message "/name" [Osc.AsciiString (Osc.ascii ty), Osc.Int32 n]

name_reply_unpack :: Osc.Message -> Maybe (String, String)
name_reply_unpack m =
  case m of
    Osc.Message "/name.reply" [Osc.AsciiString itf, Osc.AsciiString prt] ->
      Just (Osc.ascii_to_string itf, Osc.ascii_to_string prt)
    _ -> Nothing

name_reply_unpack_err :: Osc.Message -> (String, String)
name_reply_unpack_err = fromMaybe (error "name_reply_unpack") . name_reply_unpack

status_msg :: Osc.Message
status_msg = Osc.Message "/status" []

status_reply_unpack :: Osc.Message -> Maybe (Int32, Int32)
status_reply_unpack m =
  case m of
    Osc.Message "/status.reply" [Osc.Int32 i, Osc.Int32 o] -> Just (i, o)
    _ -> Nothing

status_reply_unpack_err :: Osc.Message -> (Int32, Int32)
status_reply_unpack_err = fromMaybe (error "status_reply_unpack") . status_reply_unpack

reset_msg :: Osc.Message
reset_msg = Osc.Message "/reset" []

-- * Processor

-- | Stateful infinite recursion.
recurM :: Monad m => (t -> m t) -> t -> m b
recurM act st = do
  st' <- act st
  recurM act st'

midi_osc_processor_st :: (MidiOsc_Fd -> st -> Osc.Message -> IO st) -> (MidiOsc_Fd -> IO ()) -> st -> IO ()
midi_osc_processor_st proc_f end_f init_st = do
  m_fd <- midi_osc_fd
  Sound.Osc.Fd.sendMessage m_fd receive_all_msg
  let recur_f st = do
        msg <- Sound.Osc.Fd.recvMessage m_fd
        case msg of
          Nothing -> return st
          Just msg' -> proc_f m_fd st msg'
  finally (recurM recur_f init_st) (end_f m_fd >> Sound.Osc.Fd.sendMessage m_fd receive_nil_msg)

-- | Stateless & without access to midi-osc connection.
midi_osc_processor :: (Osc.Message -> IO ()) -> IO () -> IO ()
midi_osc_processor proc_f end_f =
  let f _ () m = void (proc_f m)
  in midi_osc_processor_st f (const end_f) ()

-- * Status

name_reply :: Osc.Transport m => String -> Int32 -> m [String]
name_reply ty n = do
  Osc.sendMessage (name_msg ty n)
  m <- Osc.recvMessage_err
  let (itf, prt) = name_reply_unpack_err m
  return [ty, show n, itf, prt]

status_reply :: Osc.Transport m => m (Int32, Int32)
status_reply = do
  Osc.sendMessage status_msg
  fmap status_reply_unpack_err Osc.recvMessage_err

type Status = (Int32, [[String]], Int32, [[String]])

status_read :: Osc.Transport m => m Status
status_read = do
  (src, dst) <- status_reply
  src_nm <- mapM (name_reply "source") [0 .. src - 1]
  dst_nm <- mapM (name_reply "destination") [0 .. dst - 1]
  return (src, src_nm, dst, dst_nm)

-- * Sc3

type Sc3_Fd = Osc.OscSocket

-- | Function to generate initial state, receives connection to @Sc3@.
type Midi_Init_f st = (Sc3_Fd -> IO st)

-- | Function over @Sc3@ connection, user state and 'Midi_Message'.
type Midi_Recv_f st ty = Sc3_Fd -> st -> Midi.Channel_Voice_Message ty -> IO st

-- | Parse incoming midi messages and run 'Midi_Receiver'.
midi_act :: (Num ty, Bits ty) => Midi_Recv_f st ty -> Sc3_Fd -> st -> Osc.Message -> IO st
midi_act recv_f fd st o = do
  let m = Sound.Midi.Osc.osc_to_cvm_generic o
  recv_f fd st m

-- | Connect to @midi-osc-daemon@ and @scsynth@, run initialiser, run receiver for incoming messages.
run_midi :: (Num ty, Bits ty) => Midi_Init_f st -> Midi_Recv_f st ty -> IO ()
run_midi init_f recv_f = do
  sc3_fd <- Osc.openOscSocket (Osc.Tcp, "127.0.0.1", 57110) -- scsynth
  init_st <- init_f sc3_fd
  let proc_f _ st m = midi_act recv_f sc3_fd st m
  midi_osc_processor_st proc_f (\_ -> return ()) init_st
