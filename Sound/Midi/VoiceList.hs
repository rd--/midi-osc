-- | Very simple list model for voice allocation.
module Sound.Midi.VoiceList where

import Data.List {- base -}

type Id = Int
type Key = Int

{- | Store voices as list of required number of places.
Each place is either Nothing if the slot is free, or the key number if allocated.
-}
type VoiceList = [Maybe Key]

{- | Set the entry at index to key.

>>> setEntry(replicate 8 Nothing) 5 (Just 7)
[Nothing,Nothing,Nothing,Nothing,Nothing,Just 7,Nothing,Nothing]

>>> setEntry(replicate 8 Nothing) 7 (Just 5)
[Nothing,Nothing,Nothing,Nothing,Nothing,Nothing,Nothing,Just 5]
-}
setEntry :: VoiceList -> Id -> Maybe Key -> VoiceList
setEntry voiceList index key =
  let f index' value =
        if index == index'
          then key
          else value
  in zipWith f [0 ..] voiceList

{- | Allocate Id, if one is available.

>>> allocId [Just 4,Nothing,Nothing] 7
Just (1,[Just 4,Just 7,Nothing])
-}
allocId :: VoiceList -> Key -> Maybe (Id, VoiceList)
allocId voiceList key =
  case findIndex (== Nothing) voiceList of
    Just index -> Just (index, setEntry voiceList index (Just key))
    Nothing -> Nothing

{- | Read Id

>>> readId [Just 4,Just 7,Nothing,Nothing] 7
Just 1
-}
readId :: VoiceList -> Key -> Maybe Id
readId voiceList key =
  case findIndex (== (Just key)) voiceList of
    Just index -> Just index
    Nothing -> Nothing

{- | Free Id

>>> freeId [Just 4,Just 7,Nothing,Nothing] 7
Just (1,[Just 4,Nothing,Nothing,Nothing])
-}
freeId :: VoiceList -> Key -> Maybe (Id, VoiceList)
freeId voiceList key =
  case findIndex (== (Just key)) voiceList of
    Just index -> Just (index, setEntry voiceList index Nothing)
    Nothing -> Nothing
