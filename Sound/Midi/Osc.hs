-- | Midi Osc (Open Sound Control)
module Sound.Midi.Osc where

import Control.Monad {- base -}
import Data.Bits {- base -}
import Data.Maybe {- base -}

import qualified Data.ByteString.Lazy as ByteString {- bytestring -}

import qualified Sound.Osc.Core as Osc {- hosc -}
import qualified Sound.Osc.Text as Osc {- hosc -}

import qualified Sound.Midi.Common as Common {- midi-osc -}
import Sound.Midi.Type {- midi-osc -}

{- | Parse Osc message with address "/midi" to MidiData byte sequence.
  There are two possible encodings: a single 'Osc.MidiData' value, or an ('Osc.Int32','Osc.Blob') pair.
  Both include a port identifier, which is discarded.
-}
osc_to_bytes :: Osc.Message -> Maybe (Byte, [Byte])
osc_to_bytes m =
  case m of
    Osc.Message "/midi" [Osc.Midi (Osc.MidiData p st d1 d2)] -> Just (fromIntegral p, map fromIntegral [st, d1, d2])
    Osc.Message "/midi" [Osc.Int32 p, Osc.Blob b] -> Just (fromIntegral p, map fromIntegral (ByteString.unpack b))
    _ -> Nothing

-- | Post-process 'osc_to_bytes' to 'Cvm3' or 'SysEx'.
osc_to_cvm3_or_sysex :: Osc.Message -> Maybe (Either (Cvm3 Byte) (SysEx Byte))
osc_to_cvm3_or_sysex m =
  case osc_to_bytes m of
    Just (_, [st, d1]) -> fmap Left (cvm3_validate (st, d1, 0))
    Just (_, [st, d1, d2]) -> fmap Left (cvm3_validate (st, d1, d2))
    Just (_, x) -> if is_system_exclusive x then Just (Right x) else Nothing
    Nothing -> Nothing

-- | 'osc_to_cvm3_or_sysex' selecting only Cvm3.
osc_to_cvm3 :: Osc.Message -> Maybe (Cvm3 Byte)
osc_to_cvm3 = either Just (const Nothing) <=< osc_to_cvm3_or_sysex

-- | Erroring variant.
osc_to_cvm3_err :: Osc.Message -> Cvm3 Byte
osc_to_cvm3_err m =
  fromMaybe
    (error ("osc_to_cvm3: " ++ Osc.showMessage (Just 4) m))
    (osc_to_cvm3 m)

-- | Encode 'Cvm3', /k/ is a port-identifier.
cvm3_to_osc :: Byte -> Cvm3 Byte -> Osc.Message
cvm3_to_osc k (st, d1, d2) =
  let cast = fromIntegral
  in Osc.Message "/midi" [Osc.Midi (Osc.MidiData (cast k) (cast st) (cast d1) (cast d2))]

-- * Channel Voice Message

-- | Variant of 'osc_to_cvm3' that constructs a 'Channel_Voice_Message'.
osc_to_cvm_generic :: (Num i, Bits i) => Osc.Message -> Channel_Voice_Message i
osc_to_cvm_generic = cvm3_to_cvm . cvm3_coerce Common.int_to_num . osc_to_cvm3_err

osc_to_cvm :: Osc.Message -> Channel_Voice_Message Byte
osc_to_cvm = osc_to_cvm_generic

-- | Inverse of 'osc_to_cvm'.
cvm_to_osc :: Byte -> Channel_Voice_Message Byte -> Osc.Message
cvm_to_osc src = cvm3_to_osc src . cvm_to_cvm3

-- * MidiData Message

-- | Variant of 'osc_to_cvm3_or_sysex' that constructs a 'Midi_Message'.
osc_to_mm_generic :: (Num i, Bits i) => Osc.Message -> Midi_Message i
osc_to_mm_generic m =
  case osc_to_cvm3_or_sysex m of
    Just (Left x) -> Cvm (cvm3_to_cvm (cvm3_coerce Common.int_to_num x))
    Just (Right x) -> Scm (System_Exclusive (map Common.int_to_num x))
    Nothing -> error ("osc_to_mm: " ++ Osc.showMessage (Just 4) m)

osc_to_mm :: Osc.Message -> Midi_Message Byte
osc_to_mm = osc_to_mm_generic

-- | 'ByteString.pack' of 'mm_to_cvm3'.
mm_pack :: Midi_Message Byte -> ByteString.ByteString
mm_pack = ByteString.pack . map fromIntegral . encode_midi_message

-- | Inverse of 'osc_to_mm'.
mm_to_osc :: Byte -> Midi_Message Byte -> Osc.Message
mm_to_osc src m = Osc.Message "/midi" [Osc.int32 src, Osc.Blob (mm_pack m)]

-- * FMidiData (Fractional MidiData)

-- | Encode a fractional Cvm3 value to @midi-osc@ message.
encode_fmidi3 :: Real n => Cvm3 n -> Osc.Message
encode_fmidi3 (st, d1, d2) = Osc.Message "/fmidi3" [Osc.int32 st, Osc.float d1, Osc.float d2]

{- | Decode "/fmidi3" message.

> decode_fmidi3 (encode_fmidi3 (0x90,69.5,64.0)) == Just (0x90,69.5,64.0)
-}
decode_fmidi3_generic :: (Floating n, RealFrac n) => Osc.Message -> Maybe (Cvm3 n)
decode_fmidi3_generic m =
  let d_to_f = fromMaybe (error "decode_fmidi3") . Osc.datum_floating
  in case m of
      Osc.Message "/fmidi3" [st, d1, d2] -> Just (round (d_to_f st), d_to_f d1, d_to_f d2)
      _ -> Nothing

decode_fmidi3 :: Osc.Message -> Maybe (Cvm3 Double)
decode_fmidi3 = decode_fmidi3_generic

{- | Parse either "/fmidi3" message or standard "/midi" message.

> mapMaybe osc_to_fmidi3 [cvm3_to_osc 0 (0x90,69,64),encode_fmidi3 (0x90,69.5,64.0)]
-}
osc_to_fmidi3 :: Osc.Message -> Maybe (Cvm3 Double)
osc_to_fmidi3 m =
  maybe
    (fmap (cvm3_coerce fromIntegral) (osc_to_cvm3 m))
    Just
    (decode_fmidi3 m)
