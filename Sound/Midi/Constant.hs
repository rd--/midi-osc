-- | Midi Constants
module Sound.Midi.Constant where

{- | Byte indicating start of SYSEX message.

>>> :set -XBinaryLiterals
>>> (k_SysEx_Status == 0xF0,k_SysEx_Status == 0b11110000)
(True,True)
-}
k_SysEx_Status :: Num i => i
k_SysEx_Status = 0xF0

{- | Byte indicating end of SYSEX message.

>>> (k_SysEx_End == 0xF7,k_SysEx_End == 0b11110111)
(True,True)
-}
k_SysEx_End :: Num i => i
k_SysEx_End = 0xF7

k_Non_Real_Time :: Num i => i
k_Non_Real_Time = 0x7E

k_Real_Time :: Num i => i
k_Real_Time = 0x7F

k_Disregard_Channel :: Num i => i
k_Disregard_Channel = 0x7F

-- | Sub-Id #1
k_General_Information :: Num i => i
k_General_Information = 0x06

{- | Roland manufacturers Id.

>>> (k_Roland_Id == 0x41,k_Roland_Id == 0b01000001)
(True,True)
-}
k_Roland_Id :: Num i => i
k_Roland_Id = 0x41

{- | Yamaha manufacturer Id.

>>> (k_Yamaha_Id == 67,k_Yamaha_Id == 0x43,k_Yamaha_Id == 0b01000011)
(True,True,True)
-}
k_Yamaha_Id :: Num t => t
k_Yamaha_Id = 0x43

-- | Active sense message (System Real-Time Message, no data).
k_Active_Sense :: Num i => i
k_Active_Sense = 0xFE

-- | Data entry MSB, for RPN and NRPN
k_Data_Entry_MSB :: Num i => i
k_Data_Entry_MSB = 6

-- | Data entry LSB, for RPN and NRPN
k_Data_Entry_LSB :: Num i => i
k_Data_Entry_LSB = 38

-- * SysEx

-- | Sub-Id #2
k_Identity_Request :: Num i => i
k_Identity_Request = 0x01

-- | Sub-Id #2
k_Identity_Reply :: Num i => i
k_Identity_Reply = 0x02

-- | Sub-Id #1
k_Device_Control :: Num i => i
k_Device_Control = 0x04

-- | Sub-Id #2
k_Master_Volume :: Num i => i
k_Master_Volume = 0x01

k_Master_Balance :: Num i => i
k_Master_Balance = 0x02

k_Master_Fine_Tuning :: Num i => i
k_Master_Fine_Tuning = 0x03

k_Master_Coarse_Tuning :: Num i => i
k_Master_Coarse_Tuning = 0x04

k_Global_Parameter_Control :: Num i => i
k_Global_Parameter_Control = 0x05
