{- | Rpn (Registered Parameter Number) and Nrpn (Non-Registered Parameter Number)

<http://www.philrees.co.uk/nrpnq.htm>
-}
module Sound.Midi.Nrpn where

import qualified Sound.Midi.Constant as Constant {- midi-osc -}
import Sound.Midi.Type {- midi-osc -}

-- * Pn

-- | Function to encode Param-Id.
type Pn_Encode_K = Channel -> (Byte, Byte) -> (Cvm3 Byte, Cvm3 Byte)

-- | Encode parameter-number /k/ using controller numbers (c1,c2).
pn_encode_k :: (Byte, Byte) -> Pn_Encode_K
pn_encode_k (c1, c2) ch (k_lsb, k_msb) =
  let st = status_combine (ch, 0xB)
  in ((st, c1, k_msb), (st, c2, k_lsb))

-- | Encode U7 value, ie. Cc-6.
pn_encode_v_d1 :: Channel -> ByteWithParityBit -> Cvm3 Byte
pn_encode_v_d1 ch x = (status_combine (ch, 0xB), Constant.k_Data_Entry_MSB, x)

-- | Encode U7 pair, ie. Cc-6 & Cc-38.
pn_encode_v_d2 :: Channel -> (ByteWithParityBit, ByteWithParityBit) -> (Cvm3 Byte, Cvm3 Byte)
pn_encode_v_d2 ch (x_lsb, x_msb) =
  let st = status_combine (ch, 0xB)
  in ((st, Constant.k_Data_Entry_MSB, x_msb), (st, Constant.k_Data_Entry_LSB, x_lsb))

-- | Encode Pn setting 7-bit value (3-Cvm).
pn_encode_d1 :: Pn_Encode_K -> Channel -> (Byte, Byte) -> Byte -> [Cvm3 Byte]
pn_encode_d1 k_f ch k v =
  let (m1, m2) = k_f ch k
      d1 = pn_encode_v_d1 ch v
  in [m1, m2, d1]

-- | Encode Pn setting 14-bit value (4-Cvm).
pn_encode_d2 :: Pn_Encode_K -> Channel -> (Byte, Byte) -> (Byte, Byte) -> [Cvm3 Byte]
pn_encode_d2 k_f ch k v =
  let (m1, m2) = k_f ch k
      (d1, d2) = pn_encode_v_d2 ch v
  in [m1, m2, d1, d2]

-- * Rpn

-- | Rpn is 'pn_encode_k' of (0x65,0x64)=(101,100).
rpn_encode_k :: Pn_Encode_K
rpn_encode_k = pn_encode_k (0x65, 0x64)

-- | [(Name,Param-Id,N-Data)]
rpn_tbl :: [(String, (Byte, Byte), Int)]
rpn_tbl =
  [ ("Pitch Bend Sensitivity", (0, 0), 2) -- (Semitones,Cents)
  , ("Master Fine Tuning", (0, 1), 2) -- (Cents-A,Cents-B) (0=-100;8192=0;16383=+100)
  , ("Master Coarse Tuning", (0, 2), 1) -- (Semitones,_) (0=-64;64=0;127=+63)
  , ("Tuning Program Select", (0, 3), 1) -- (Ix,_)
  , ("Tuning Bank Select", (0, 4), 1) -- (Ix,_)
  , ("Null", (127, 127), 0) -- (_,_)
  ]

-- * Nrpn

-- | Nrpn is 'pn_encode_k' of (0x63,0x62)=(99,98).
nrpn_encode_k :: Pn_Encode_K
nrpn_encode_k = pn_encode_k (0x63, 0x62)
