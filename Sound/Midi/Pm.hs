-- | PortMidi <http://portmedia.sourceforge.net/>
module Sound.Midi.Pm where

import Control.Exception {- base -}
import Control.Monad {- base -}
import Data.Bits {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Foreign.C.Types {- base -}
import System.Environment {- base -}

import qualified Sound.PortMidi as PortMidi {- portmidi -}

import qualified Sound.Osc as Osc {- hosc -}

import Sound.Midi.Type {- midi-osc -}

-- * Device Id

-- | Portmidi device identifier.
type Pm_Dev = PortMidi.DeviceID

-- | Generate device table.
pm_enumerate_devices :: IO [(Pm_Dev, PortMidi.DeviceInfo)]
pm_enumerate_devices = do
  n <- PortMidi.countDevices
  let k = [0 .. n - 1]
  i <- mapM PortMidi.getDeviceInfo k
  return (zip k i)

-- | Table (Header,Data) of `pm_enumerate_devices`.
pm_device_tbl :: IO ([String], [[String]])
pm_device_tbl = do
  let hdr = words "ID INTERFACE NAME INPUT OUTPUT OPEN"
      f (k, PortMidi.DeviceInfo itf nm inp out opn) = [show k, itf, nm, show inp, show out, show opn]
  dv <- pm_enumerate_devices
  return (hdr, map f dv)

-- | Select devices by predicate.
pm_select_id :: (PortMidi.DeviceInfo -> Bool) -> IO [Pm_Dev]
pm_select_id f =
  let g (k, i) = if f i then Just k else Nothing
  in fmap (mapMaybe g) pm_enumerate_devices

-- | Select input devices.
pm_all_input_id :: IO [Pm_Dev]
pm_all_input_id = pm_select_id PortMidi.input

-- | Select output devices.
pm_all_output_id :: IO [Pm_Dev]
pm_all_output_id = pm_select_id PortMidi.output

-- | Find device by predicate.  Returns first match from 'pm_enumerate_devices'.
pm_find_id :: (PortMidi.DeviceInfo -> Bool) -> IO Pm_Dev
pm_find_id f = do
  d <- pm_enumerate_devices
  case find (f . snd) d of
    Nothing -> error "pm_find_id"
    Just r -> return (fst r)

pm_is_named_output :: String -> PortMidi.DeviceInfo -> Bool
pm_is_named_output nm i = PortMidi.name i == nm && PortMidi.output i

{- | Find output device by name.

> k <- pm_output_by_name "Midi Through Port-0"
> PortMidi.getDeviceInfo k
-}
pm_output_by_name :: String -> IO Pm_Dev
pm_output_by_name = pm_find_id . pm_is_named_output

-- | Get device named in environment variable @PM_DEFAULT_OUTPUT@.
pm_default_output :: IO Pm_Dev
pm_default_output = getEnv "PM_DEFAULT_OUTPUT" >>= pm_output_by_name

pm_is_named_input :: String -> PortMidi.DeviceInfo -> Bool
pm_is_named_input nm i = PortMidi.name i == nm && PortMidi.input i

{- | Find input device by name.

> k <- pm_input_by_name "UA-25 MIDI 1"
> PortMidi.getDeviceInfo k
-}
pm_input_by_name :: String -> IO Pm_Dev
pm_input_by_name = pm_find_id . pm_is_named_input

-- | Get device named in environment variable @PM_DEFAULT_INPUT@.
pm_default_input :: IO Pm_Dev
pm_default_input = getEnv "PM_DEFAULT_INPUT" >>= pm_input_by_name

-- * Msg

-- | Portmidi stores messages in 32-bit blocks.
type Pm_I32 = CLong

-- | Message unpacked to 4-byte tuple.
type Pm_Msg = (Byte, Byte, Byte, Byte)

-- | Unpack 'Pm_I32' to 'Pm_Msg'
pm_msg_decode :: Pm_I32 -> Pm_Msg
pm_msg_decode i =
  let f = fromIntegral
  in ( f (i .&. 0xFF)
     , f (shiftR i 8) .&. 0xFF
     , f (shiftR i 16) .&. 0xFF
     , f (shiftR i 24) .&. 0xFF
     )

-- | Pack 'Pm_Msg' to 'Pm_I32'.
pm_msg_encode :: Pm_Msg -> Pm_I32
pm_msg_encode (s, d1, d2, d3) =
  let f = fromIntegral
  in f (shiftL (d3 .&. 0xFF) 24)
      .|. f (shiftL (d2 .&. 0xFF) 16)
      .|. f (shiftL (d1 .&. 0xFF) 8)
      .|. f (s .&. 0xFF)

-- | Unpack 'Pm_I32' to list.
pm_msg_decode_list :: Pm_I32 -> [Byte]
pm_msg_decode_list m = let (p, q, r, s) = pm_msg_decode m in [p, q, r, s]

-- * Error

type Pm_Result = Either PortMidi.PMError PortMidi.PMSuccess

pm_NoError :: PortMidi.PMSuccess
pm_NoError = PortMidi.NoError'NoData

pm_error_pp :: String -> PortMidi.PMError -> String
pm_error_pp own err = show (own, err)

pm_on_error :: String -> PortMidi.PMError -> t
pm_on_error own = error . pm_error_pp own

-- | Do nothing if 'PortMidi.PMSuccess', else 'error'.
pm_handle_error :: Pm_Result -> IO ()
pm_handle_error x =
  case x of
    Right _ -> return ()
    Left err -> pm_on_error "pm_handle_error" err

pm_maybe_error :: Monad m => String -> Either PortMidi.PMError x -> m x
pm_maybe_error own r =
  case r of
    Left err -> pm_on_error own err
    Right x -> return x

-- * Bracket, with, open, close

-- | 'bracket_' with 'PortMidi.initialize' and 'PortMidi.terminate'.
pm_with_midi :: IO t -> IO t
pm_with_midi = bracket_ (PortMidi.initialize >>= pm_handle_error) (PortMidi.terminate >>= pm_handle_error)

type Pm_Fd = PortMidi.PMStream

-- | Open function, there are separate functions for input and output devices.
type Open_F = Pm_Dev -> IO Pm_Fd

-- | 'PortMidi.openInput' with 'error' on failure.
pm_open_input :: Open_F
pm_open_input k = PortMidi.openInput k >>= pm_maybe_error "pm_open_input"

-- | 'pm_open_input' of 'pm_default_input'.
pm_open_input_def :: IO Pm_Fd
pm_open_input_def = pm_default_input >>= pm_open_input

-- | 'PortMidi.openOutput' with zero latency & 'error' on failure.
pm_open_output :: Open_F
pm_open_output k = PortMidi.openOutput k 0 >>= pm_maybe_error "pm_open_output"

-- | 'pm_open_output' of 'pm_default_output'
pm_open_output_def :: IO Pm_Fd
pm_open_output_def = pm_default_output >>= pm_open_output

-- | 'PortMidi.close' and 'pm_handle_error'
pm_close :: Pm_Fd -> IO ()
pm_close c = PortMidi.close c >>= pm_handle_error

-- | Open default input and output ports.
pm_open_io_def :: IO (Pm_Fd, Pm_Fd)
pm_open_io_def = do
  in_fd <- pm_open_input_def
  out_fd <- pm_open_output_def
  return (in_fd, out_fd)

-- | Bracketed default I/O ports.
pm_with_io_def :: ((Pm_Fd, Pm_Fd) -> IO t) -> IO t
pm_with_io_def = pm_with_midi . bracket pm_open_io_def (\(i, o) -> pm_close i >> pm_close o)

pm_with_device :: Open_F -> Pm_Dev -> (Pm_Fd -> IO t) -> IO t
pm_with_device f k = pm_with_midi . bracket (f k) pm_close

{- | Run function with indicated input port.

> pm_with_input_device k print
-}
pm_with_input_device :: Pm_Dev -> (Pm_Fd -> IO t) -> IO t
pm_with_input_device = pm_with_device pm_open_input

{- | Run function with indicated output port.

> pm_with_output_device 2 (\s -> pm_sysex_write s [240,65,0,20,18,0,3,25,16,84,247])
-}
pm_with_output_device :: Pm_Dev -> (Pm_Fd -> IO t) -> IO t
pm_with_output_device = pm_with_device pm_open_output

pm_with_default_output :: (Pm_Fd -> IO r) -> IO r
pm_with_default_output f = pm_default_output >>= \k -> pm_with_output_device k f

pm_with_device_set :: Open_F -> [Pm_Dev] -> ([Pm_Fd] -> IO t) -> IO t
pm_with_device_set f k =
  pm_with_midi
    . bracket (mapM f k) (mapM_ (PortMidi.close >=> pm_handle_error))

pm_with_input_device_set :: [Pm_Dev] -> ([Pm_Fd] -> IO t) -> IO t
pm_with_input_device_set = pm_with_device_set pm_open_input

-- * I/O

-- | Run 'PortMidi.poll' to see if there are input events queued.
pm_events_queued :: Pm_Fd -> IO Bool
pm_events_queued fd = do
  r <- PortMidi.poll fd
  let b = case r of
        Left _ -> False
        Right PortMidi.GotData -> True
        Right _ -> False
  return b

-- | Wait (pause thread) for /ms/ milli-seconds.
pm_pause :: Double -> IO ()
pm_pause ms = Osc.pauseThread (ms / 1000)

-- | Wait until there are input events to read.  /ms/ is delay-time in milli-seconds.
pm_wait_for_input :: Double -> Pm_Fd -> IO ()
pm_wait_for_input ms fd =
  let recur = do
        q <- pm_events_queued fd
        if q then return () else pm_pause ms >> recur
  in recur

-- | Read all queued 'Pm_I32'.
pm_read_all_queued :: PortMidi.PMStream -> IO [Pm_I32]
pm_read_all_queued fd =
  let recur x = do
        r <- PortMidi.readEvents fd
        case r of
          Left err -> error (show err)
          Right [] -> return x
          Right m -> recur (x ++ map PortMidi.message m)
  in recur []

-- | Wait for input, read input events, unpack channel voice messages.
pm_read_cvm3 :: Pm_Fd -> IO [Either (Cvm3 Byte) Pm_I32]
pm_read_cvm3 fd = do
  pm_wait_for_input 10 fd
  m <- pm_read_all_queued fd
  let f x =
        let (st, d1, d2, _) = pm_msg_decode x
        in if is_channel_voice_message st then Left (st, d1, d2) else Right x
  return (map f m)

-- | Write 'Pm_I32'.
pm_write :: Pm_Fd -> Pm_I32 -> IO ()
pm_write fd msg = PortMidi.writeShort fd (PortMidi.PMEvent msg 0) >>= pm_handle_error

pm_write_cvm3 :: Pm_Fd -> Cvm3 Byte -> IO ()
pm_write_cvm3 fd (st, d1, d2) = pm_write fd (pm_msg_encode (st, d1, d2, 0))

-- * SysEx

{- | Wait for input then read a SysEx message.
  If the queued data is incomplete wait for further input until 0xF7 is received.
  Error if data is not a SysEx.
-}
pm_sysex_read :: Pm_Fd -> IO (SysEx Byte)
pm_sysex_read fd =
  let recur x = do
        pm_wait_for_input 10 fd
        m <- pm_read_all_queued fd
        let d = concatMap pm_msg_decode_list m
            d0 : _ = d
        when (null x && d0 /= 0xF0) (error "pm_read_sysex: not sysex?")
        if 0xF7 `elem` d
          then return (x ++ dropWhileEnd (/= 0xF7) d)
          else recur (x ++ d)
  in recur []

-- | 'PortMidi.writeSysEx' requires input as a 'String'.
sysex_to_string :: SysEx Byte -> String
sysex_to_string = map toEnum

-- | Write system exclusive message.
pm_sysex_write :: Pm_Fd -> SysEx Byte -> IO ()
pm_sysex_write fd m = do
  t <- PortMidi.time
  PortMidi.writeSysEx fd t (sysex_to_string m) >>= pm_handle_error

-- | Write sequence of SysEx with /ms/ delays between writes.
pm_sysex_write_seq :: Double -> Pm_Fd -> [SysEx Byte] -> IO ()
pm_sysex_write_seq ms fd =
  let recur sq =
        case sq of
          [] -> return ()
          m : sq' ->
            pm_sysex_write fd m
              >> pm_pause ms
              >> recur sq'
  in recur

-- * Cvm3

-- | Unpack 'Pm_I32' to 'Cvm3'.
pm_decode_cvm3 :: Pm_I32 -> Maybe (Cvm3 Byte)
pm_decode_cvm3 m =
  let (st, d1, d2, _) = pm_msg_decode m
  in if is_channel_voice_message st
      then Just (fromIntegral st, fromIntegral d1, fromIntegral d2)
      else Nothing

-- | Error if not channel-voice-message.
pm_decode_cvm3_err :: Pm_I32 -> Cvm3 Byte
pm_decode_cvm3_err = fromMaybe (error "pm_decode_cvm3") . pm_decode_cvm3

-- | Pack 'Cvm3' into 'Pm_I32'.
pm_encode_cvm3 :: Cvm3 Byte -> Pm_I32
pm_encode_cvm3 (st, d1, d2) = pm_msg_encode (st, d1, d2, 0)

-- | Write 'Cvm3' with zero timestamp.
pm_cvm3_write :: Pm_Fd -> Cvm3 Byte -> IO ()
pm_cvm3_write fd msg = PortMidi.writeShort fd (PortMidi.PMEvent (pm_encode_cvm3 msg) 0) >>= pm_handle_error

-- | 'pm_cvm3_write' to all indicated outputs.
pm_cvm3_broadcast :: [Pm_Fd] -> Cvm3 Byte -> IO ()
pm_cvm3_broadcast fd_set msg = mapM_ (`pm_cvm3_write` msg) fd_set

-- | Write sequence of 'Cvm3' with zero timestamp.
pm_cvm3_write_seq :: Pm_Fd -> [Cvm3 Byte] -> IO ()
pm_cvm3_write_seq fd msg_seq =
  let ev = map (\msg -> PortMidi.PMEvent (pm_encode_cvm3 msg) 0) msg_seq
  in PortMidi.writeEvents fd ev >>= pm_handle_error

-- | Broadcast variant of 'pm_cvm3_write_seq'.
pm_cvm3_broadcast_seq :: [Pm_Fd] -> [Cvm3 Byte] -> IO ()
pm_cvm3_broadcast_seq fd_set msg_seq = mapM_ (`pm_cvm3_write_seq` msg_seq) fd_set

-- * Channel_Voice_Message

pm_decode_cvm :: Pm_I32 -> Channel_Voice_Message Byte
pm_decode_cvm = cvm3_to_cvm . pm_decode_cvm3_err

pm_encode_cvm :: Channel_Voice_Message Byte -> Pm_I32
pm_encode_cvm = pm_encode_cvm3 . cvm_to_cvm3

-- * Processor

-- | Message processor function, in IO monad.
type Proc_F = Either (Cvm3 Byte) Pm_I32 -> IO ()

-- | Returns 'True' if there were any events to process, else 'False'.
pm_process_events :: Proc_F -> Pm_Fd -> IO Bool
pm_process_events proc_f fd = do
  q <- pm_events_queued fd
  if not q then return False else pm_read_cvm3 fd >>= mapM_ proc_f >> return True

{- | Process input from all input devices.
  When there are no pending events pause for ms milli-seconds.

> pm_midi_processor 1 print
-}
pm_midi_processor :: Double -> Proc_F -> IO ()
pm_midi_processor ms proc_f = do
  k <- pm_all_input_id
  let go fd = do
        r <- mapM (pm_process_events proc_f) fd
        when (all (== False) r) (pm_pause ms)
        go fd
  pm_with_input_device_set k go
