-- | Midi common
module Sound.Midi.Common where

import Data.Bifunctor {- base -}
import Data.Bits {- base -}

import qualified Data.List.Split as Split {- split -}

import qualified Data.ByteString as B {- bytestring -}

-- | Type-specialised 'fromIntegral'.
int_to_num :: Num n => Int -> n
int_to_num = fromIntegral

-- | Type-specialised 'truncate'.
double_to_int :: Double -> Int
double_to_int = truncate

-- | 'double_to_int' of 'realToFrac'.
real_to_int :: Real n => n -> Int
real_to_int = double_to_int . realToFrac

-- | Most-significant 4-bits of U8.
bits8_hi :: (Bits t, Num t) => t -> t
bits8_hi x = shiftR x 4

-- | Least-significant 4-bits of U8.
bits8_lo :: (Bits t, Num t) => t -> t
bits8_lo x = x .&. 0x0F

{- | Separate U8 into high and low 4-bit values.

>>> bits8_sep 0x9A == (0x9,0xA)
True
-}
bits8_sep :: (Bits t, Num t) => t -> (t, t)
bits8_sep x = (shiftR x 4, x .&. 0x0F)

{- | Join high and low 4-bit values into 8-bit value (most-significant,least-signficant)

>>> bits4_join (0x9,0xA) == 0x9A
True
-}
bits4_join :: Bits t => (t, t) -> t
bits4_join (d1, d2) = shiftL d1 4 .|. d2

{- | Join two 7-bit numbers (no bit shifting) (lsb,msb)

>>> map bits_7_join [(0,0),(0x40,0),(0,0x01),(0,0x40),(0x7F,0x7F)] == [0,0x40,0x80,0x2000,0x3FFF]
True

>>> map bits_7_join [(0,0),(64,0),(0,1),(0,64),(127,127)] == [0,64,128,8192,16383]
True
-}
bits_7_join :: Num a => (a, a) -> a
bits_7_join (p, q) = p + (q * 128)

{- | Given divide/floor function, seperate 14-bit number into two 7-bit numbers (lsb,msb)

>>> divFloor i j = fromIntegral (floor (i / j))
>>> map (bits_14_sep_f divFloor) [0,64,128,8192,16383]
[(0.0,0.0),(64.0,0.0),(0.0,1.0),(0.0,64.0),(127.0,127.0)]
-}
bits_14_sep_f :: Num t => (t -> t -> t) -> t -> (t, t)
bits_14_sep_f f x = let q = f x 128 in (x - (q * 128), q)

{- | 'bits_14_sep_f' of 'div'

>>> map bits_14_sep_int [0,64,128,8192,16383]
[(0,0),(64,0),(0,1),(0,64),(127,127)]
-}
bits_14_sep_int :: Integral i => i -> (i, i)
bits_14_sep_int = bits_14_sep_f div

-- * Bits (LE = little-endian, BE = big-endian)

-- | Join two 7-bit values into a 14-bit value, (lsb,msb)
bits_7_join_le :: Bits t => (t, t) -> t
bits_7_join_le (p, q) = p .|. shiftL q 7

-- | Inverse of 'bits_14_sep_le', (lsb,msb)
bits_14_sep_le :: (Bits t, Num t) => t -> (t, t)
bits_14_sep_le n = (0x7F .&. n, 0xFF .&. shiftR n 7)

{- | 'fromIntegral' of 'bits_7_join' of 'real_to_int'.

>>> map fbits_7_join_le [(0.0,0.0),(64.0,0.0),(0.0,64.0),(127.0,127.0)]
[0.0,64.0,8192.0,16383.0]
-}
fbits_7_join_le :: RealFrac f => (f, f) -> f
fbits_7_join_le = fromIntegral . bits_7_join_le . bimap real_to_int real_to_int

{- | 'fromIntegral' of 'bits_14_sep' of 'real_to_int'.

>>> map fbits_14_sep_le [0.0,64.0,8192.0,16383.0]
[(0.0,0.0),(64.0,0.0),(0.0,64.0),(127.0,127.0)]
-}
fbits_14_sep_le :: RealFrac f => f -> (f, f)
fbits_14_sep_le = bimap fromIntegral fromIntegral . bits_14_sep_le . real_to_int

{- | Join three 7-bit values into a 21-bit value, MSB -> LSB.

>>> bits_21_sep_be 128 == (0x00,0x01,0x00)
True

>>> bits_21_sep_be 192 == (0x00,0x01,0x40)
True

>>> bits_21_sep_be 320 == (0x00,0x02,0x40)
True

>>> bits_21_sep_be 421 == (0x00,0x03,0x25)
True

>>> bits_21_sep_be (2 ^ 14) == (1,0,0)
True
-}
bits_21_sep_be :: (Bits t, Num t) => t -> (t, t, t)
bits_21_sep_be a =
  ( shiftR a 14 .&. 0x7F
  , shiftR a 7 .&. 0x7F
  , a .&. 0x7F
  )

{- | Inverse of 'bits_21_sep_be'.

>>> bits_21_join_be (0x00,0x01,0x00)
128

>>> bits_21_join_be (0x00,0x01,0x40)
192

>>> bits_21_join_be (0x00,0x02,0x40)
320

>>> bits_21_join_be (0x00,0x03,0x19)
409

>>> bits_21_join_be (0x00,0x03,0x25)
421
-}
bits_21_join_be :: (Bits t, Num t) => (t, t, t) -> t
bits_21_join_be (p, q, r) = shiftL p 14 .|. shiftL q 7 .|. r

{- | Little-endian variant

>>> bits_21_join_le (0x19,0x03,0x00)
409
-}
bits_21_join_le :: (Bits t, Num t) => (t, t, t) -> t
bits_21_join_le (r, q, p) = bits_21_join_be (p, q, r)

-- * Segment

{- | Segment sequence, segments begin with /e/.
   This can segment a sequence of Sysex messages, which begin with '0xF0'.

>>> segment_at 'x' "xxyxyz"
["x","xy","xyz"]
-}
segment_at :: Eq t => t -> [t] -> [[t]]
segment_at e x =
  let split_before :: Eq a => a -> [a] -> [[a]]
      split_before = Split.split . Split.keepDelimsL . Split.oneOf . return
  in case split_before e x of
      [] : r -> r
      _ -> error "segment_at?"

-- * Io

-- | Read binary byte sequence from file.
bytes_load :: Integral t => FilePath -> IO [t]
bytes_load = fmap (map fromIntegral . B.unpack) . B.readFile

-- | Write binary byte sequence to file.
bytes_store :: Integral t => FilePath -> [t] -> IO ()
bytes_store fn = B.writeFile fn . B.pack . map fromIntegral
