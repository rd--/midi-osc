-- | Midi 'Key' functions.
module Sound.Midi.Ky where

import Data.Maybe {- base -}

import qualified Data.Map as Map {- containers -}

import qualified Sound.Sc3 as Sc3 {- hsc3 -}

{- | Map of allocated 'Sc3.Node_Id's.

For a single input controller, key events always arrive in sequence
(ie. on->off), ie. for any key on message we can allocate a set of Id
and associate it with the key, an off message can retrieve the set of
Id given the key.

Note: For input controllers with octave shift keys (ie. AKAI MPK mini)
we may see two consecutive on events for the same key.
-}
data Ky a = Ky (Map.Map a [Sc3.Node_Id]) Sc3.Node_Id deriving (Show)

-- | Initialise 'Ky' with starting 'Sc3.Node_Id'.
ky_init :: Sc3.Node_Id -> Ky a
ky_init = Ky Map.empty

-- | 'Ky' 'Sc3.Node_Id' allocator, allocates /k/ ids.
ky_alloc :: Ord a => Ky a -> a -> Int -> Maybe (Ky a, [Sc3.Node_Id])
ky_alloc (Ky m i) n k =
  case Map.lookup n m of
    Just _ -> Nothing
    Nothing ->
      let sq = [i .. i + k - 1]
      in Just (Ky (Map.insert n sq m) (i + k), sq)

ky_alloc_nil :: Ord a => Ky a -> a -> Int -> (Ky a, [Sc3.Node_Id])
ky_alloc_nil ky n = fromMaybe (ky, []) . ky_alloc ky n

ky_alloc_err :: Ord a => Ky a -> a -> Int -> (Ky a, [Sc3.Node_Id])
ky_alloc_err ky n = fromMaybe (error "ky_alloc") . ky_alloc ky n

-- | Re-assign an existing key.
ky_reassign1 :: Ord a => Ky a -> a -> Sc3.Node_Id -> Ky a
ky_reassign1 (Ky m i) k v = Ky (Map.insert k [v] m) i

-- | Special case to allocate singular ID.
ky_alloc1 :: Ord a => Ky a -> a -> (Ky a, Sc3.Node_Id)
ky_alloc1 (Ky m i) n =
  case Map.lookup n m of
    Just x -> error ("ky_alloc1: key exists: " ++ show x)
    Nothing -> (Ky (Map.insert n [i] m) (i + 1), i)

-- | 'Ky' 'Sc3.Node_Id' removal, returns ids removed.
ky_free :: Ord a => Ky a -> a -> Maybe (Ky a, [Sc3.Node_Id])
ky_free (Ky m i) n =
  case Map.lookup n m of
    Just r -> Just (Ky (Map.delete n m) i, r)
    Nothing -> Nothing

-- | Variant that gives empty set for non-existing key.
ky_free_nil :: Ord a => Ky a -> a -> (Ky a, [Sc3.Node_Id])
ky_free_nil ky = fromMaybe (ky, []) . ky_free ky

-- | Variant that errors for non-existing key.
ky_free_err :: Ord a => Ky a -> a -> (Ky a, [Sc3.Node_Id])
ky_free_err ky = fromMaybe (error "ky_free") . ky_free ky

-- | Special case to de-allocate singular ID.
ky_free1 :: Ord a => Ky a -> a -> (Ky a, Maybe Sc3.Node_Id)
ky_free1 ky n =
  case ky_free ky n of
    Nothing -> (ky, Nothing)
    Just (ky', [x]) -> (ky', Just x)
    _ -> error "ky_free1: not singular"

-- | Error variant.
ky_free1_err :: Ord a => Ky a -> a -> (Ky a, Sc3.Node_Id)
ky_free1_err ky n =
  case ky_free1 ky n of
    (_, Nothing) -> error "ky_free1_err"
    (ky', Just x) -> (ky', x)

-- | Lookup 'Sc3.Node_Id'.
ky_get :: Ord a => Ky a -> a -> [Sc3.Node_Id]
ky_get (Ky m _) n = m Map.! n

-- | All 'Sc3.Node_Id'.
ky_all :: Ky a -> [Sc3.Node_Id]
ky_all (Ky m _) = Map.foldl (++) [] m
