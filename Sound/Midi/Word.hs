-- | Midi sized word functions
module Sound.Midi.Word where

import Data.Bifunctor {- base -}
import Data.Bits {- base -}
import Data.Int {- base -}
import Data.Maybe {- base -}
import Data.Word {- base -}

import qualified Data.ByteString as B {- bytestring -}

import qualified Sound.Midi.Common as Common {- midi-osc -}

-- | Unsigned 4-bit integer.
type U4 = Word8

-- | Unsigned 8-bit integer.
type U8 = Word8

-- | Unsigned 16-bit integer.
type U16 = Word16

-- | Unsigned 32-bit integer.
type U32 = Word32

-- | Most-significant 4-bits of U8.
u8_hi :: U8 -> U4
u8_hi x = shiftR x 4

-- | Least-significant 4-bits of U8.
u8_lo :: U8 -> U4
u8_lo x = x .&. 0x0F

{- | Separate U8 into high and low 4-bit values.

>>> u8_sep 0x9A == (0x9,0xA)
True
-}
u8_sep :: U8 -> (U4, U4)
u8_sep x = (shiftR x 4, x .&. 0x0F)

{- | Join high and low 4-bit values into 8-bit value (most-significant,least-signficant)

>>> u4_join (0x9,0xA) == 0x9A
True
-}
u4_join :: (U4, U4) -> U8
u4_join (d1, d2) = shiftL d1 4 .|. d2

-- | Type-specialised 'fromIntegral'.
u8_to_u16 :: U8 -> U16
u8_to_u16 = fromIntegral

-- | Type-specialised 'fromIntegral'.
u8_to_u32 :: U8 -> U32
u8_to_u32 = fromIntegral

-- | Type-specialised 'fromIntegral'.
u8_to_int :: U8 -> Int
u8_to_int = fromIntegral

-- | Type-specialised 'fromIntegral'.
u8_to_double :: U8 -> Double
u8_to_double = fromIntegral

-- | Type-specialised 'fromIntegral'.
u8_to_num :: Num n => U8 -> n
u8_to_num = fromIntegral

-- | Range checking and type-specialised 'fromIntegral'.
u16_to_u8 :: U16 -> U8
u16_to_u8 n = if n > 255 then error "u16_to_u8" else fromIntegral n

-- | Range checking and type-specialised 'fromIntegral'.
u32_to_u8 :: U32 -> U8
u32_to_u8 n = if n > 255 then error "u32_to_u8" else fromIntegral n

-- | Type-specialised 'floor'.
double_to_u8 :: Double -> U8
double_to_u8 = floor

-- | 'double_to_u8' of 'realToFrac'.
real_to_u8 :: Real n => n -> U8
real_to_u8 = double_to_u8 . realToFrac

-- | 'toEnum' of 'u8_to_int'.
u8_to_enum :: Enum e => U8 -> e
u8_to_enum = toEnum . u8_to_int

-- | Type-specialised 'fromIntegral'
u16_to_double :: U16 -> Double
u16_to_double = fromIntegral

-- | Type-specialised 'fromIntegral'
int_to_u8 :: Int -> U8
int_to_u8 = fromIntegral

-- | 'int_to_u8' of 'fromEnum'
char_to_u8 :: Char -> U8
char_to_u8 = int_to_u8 . fromEnum

u8_to_char :: U8 -> Char
u8_to_char = toEnum . u8_to_int

-- | Type specialised 'fromIntegral' with out-of-range error
i32_to_u7 :: Int32 -> U8
i32_to_u7 x = if x < 0 || x > 127 then error "i32_to_u7: OUT-OF-RANGE" else fromIntegral x

-- * Join/Split

{- | Type-specialised 'bits_7_join_generic_le', (lsb,msb)

>>> map bits_7_join_le [(0,0),(64,0),(0,1),(0,64),(127,127)]
[0,64,128,8192,16383]
-}
bits_7_join_le :: (U8, U8) -> U16
bits_7_join_le = Common.bits_7_join_le . bimap u8_to_u16 u8_to_u16

{- | Type-specialised 'bits_14_sep_generic', (lsb,msb)

>>> map bits_14_sep_le [0,64,128,8192,16383]
[(0,0),(64,0),(0,1),(0,64),(127,127)]
-}
bits_14_sep_le :: U16 -> (U8, U8)
bits_14_sep_le = bimap u16_to_u8 u16_to_u8 . Common.bits_14_sep_le

{- | Join three 7-bit values into a 21-bit value, MSB -> LSB.

>>> bits_21_sep_be 128 == (0x00,0x01,0x00)
True

>>> bits_21_sep_be 192 == (0x00,0x01,0x40)
True

>>> bits_21_sep_be 320 == (0x00,0x02,0x40)
True

>>> bits_21_sep_be 421 == (0x00,0x03,0x25)
True

>>> bits_21_sep_be (2 ^ 14) == (1,0,0)
True
-}
bits_21_sep_be :: U32 -> (U8, U8, U8)
bits_21_sep_be a =
  ( u32_to_u8 (shiftR a 14 .&. 0x7F)
  , u32_to_u8 (shiftR a 7 .&. 0x7F)
  , u32_to_u8 (a .&. 0x7F)
  )

{- | Inverse of 'bits_21_sep_be'.

>>> bits_21_join_be (0x00,0x01,0x00)
128

>>> bits_21_join_be (0x00,0x01,0x40)
192

>>> bits_21_join_be (0x00,0x02,0x40)
320

>>> bits_21_join_be (0x00,0x03,0x19)
409

>>> bits_21_join_be (0x00,0x03,0x25)
421
-}
bits_21_join_be :: (U8, U8, U8) -> U32
bits_21_join_be (p, q, r) = shiftL (u8_to_u32 p) 14 .|. shiftL (u8_to_u32 q) 7 .|. u8_to_u32 r

-- * Assert

{- | Check that /i/ is in (0,255), required when /i/ is not actually Word8.

>>> map word8_check [-1,0,255,256]
[Nothing,Just 0,Just 255,Nothing]
-}
word8_check :: (Num t, Ord t) => t -> Maybe t
word8_check i = if i < 0 || i > 255 then Nothing else Just i

-- | Erroring variant.
word8_check_err :: (Num t, Ord t) => t -> t
word8_check_err = fromMaybe (error "word8_check") . word8_check

-- * Io

-- | Read binary U8 sequence from file.
u8_load :: FilePath -> IO [U8]
u8_load = fmap B.unpack . B.readFile

-- | Write binary U8 sequence to file.
u8_store :: FilePath -> [U8] -> IO ()
u8_store fn = B.writeFile fn . B.pack
