-- | Monitor incoming midi messages and maintain a midi state data structure.
module Sound.Midi.St where

import Control.Concurrent {- base -}

import qualified Data.Map as Map {- containers -}

import qualified Sound.Sc3.Common.Math as Sc3 {- hsc3 -}

import qualified Sound.Midi.Common as Common {- midi-osc -}
import qualified Sound.Midi.Osc.Server as Server {- midi-osc -}
import qualified Sound.Midi.Type as Type {- midi-osc -}

-- * Types

-- | Alias for 'Nibble'
type Midi_4bit = Type.Nibble

-- | Alias for 'ByteWithParityBit'
type Midi_7bit = Type.ByteWithParityBit

-- | Alias for 'Int'
type Midi_14bit = Int

-- | Midi channel number (0,15).
type Midi_Channel = Type.Channel

-- | Midi note number (0,127).
type Midi_Note_Number = Type.Key

-- | Midi velocity (0,127).
type Midi_Velocity = Type.Velocity

-- | Midi program/patch number (0,127).
type Midi_Program = Midi_7bit

-- | Continuous controller index (0,127).
type Midi_CC_Number = Midi_7bit

-- | Continuous controller value (0,127).
type Midi_CC_Value = Midi_7bit

-- | Midi pitch bend number (0,16383).
type Midi_Pitch_Bend = Midi_14bit

-- | Map of (note-number,velocity).
type Midi_Key_Map = Map.Map (Midi_Channel, Midi_Note_Number) Midi_Velocity

-- | Map of (cc-number,cc-value)
type Midi_CC_Map = Map.Map (Midi_Channel, Midi_CC_Number) Midi_CC_Value

type Midi_Program_Map = Map.Map Midi_Channel Midi_Program

type Midi_Pitch_Bend_Map = Map.Map Midi_Channel Midi_Pitch_Bend

-- | Midi state.
data Midi_State_T t = Midi_State_T
  { st_km :: Midi_Key_Map
  , st_pc :: Midi_Program_Map
  , st_cc :: Midi_CC_Map
  , st_pb :: Midi_Pitch_Bend_Map
  , st_usr :: t
  }
  deriving (Show)

-- | 'MVar' of 'Midi_State_T'
type Midi_State t = MVar (Midi_State_T t)

-- * State functions

st_modify :: (t -> t) -> MVar t -> IO ()
st_modify f mv = modifyMVar_ mv (return . f)

-- | Edit key map.  A 'Midi_Velocity' of zero indicates note-off and deletes entry.
st_edit_km :: (Midi_Channel, Midi_Note_Number, Midi_Velocity) -> Midi_State t -> IO ()
st_edit_km (ch, k, v) =
  let f st =
        let m = st_km st
            m' = if v == 0 then Map.delete (ch, k) m else Map.insert (ch, k) v m
        in st {st_km = m'}
  in st_modify f

-- | Edit controller map.
st_edit_cc :: (Midi_Channel, Midi_CC_Number, Midi_CC_Value) -> Midi_State t -> IO ()
st_edit_cc (ch, k, v) = st_modify (\st -> st {st_cc = Map.insert (ch, k) v (st_cc st)})

-- | Clear (empty) controller map.
st_clear_cc :: Midi_State t -> IO ()
st_clear_cc = st_modify (\st -> st {st_cc = Map.empty})

-- | Edit program/patch value.
st_edit_pc :: (Midi_Channel, Midi_Program) -> Midi_State t -> IO ()
st_edit_pc (ch, pc) = st_modify (\st -> st {st_pc = Map.insert ch pc (st_pc st)})

-- | Edit pitch bend value.
st_edit_pb :: (Midi_Channel, Midi_Pitch_Bend) -> Midi_State t -> IO ()
st_edit_pb (ch, pb) = st_modify (\st -> st {st_pb = Map.insert ch pb (st_pb st)})

st_replace_usr :: t -> Midi_State t -> IO ()
st_replace_usr x = st_modify (\st -> st {st_usr = x})

-- * Query functions

st_access :: (a -> b) -> MVar a -> IO b
st_access f mv = withMVar mv (return . f)

st_access_km :: (Midi_Key_Map -> r) -> Midi_State t -> IO r
st_access_km f = st_access (f . st_km)

st_access_cc :: (Midi_CC_Map -> r) -> Midi_State t -> IO r
st_access_cc f = st_access (f . st_cc)

st_access_pc :: (Midi_Program_Map -> r) -> Midi_State t -> IO r
st_access_pc f = st_access (f . st_pc)

st_access_pitch_bend :: (Midi_Pitch_Bend_Map -> r) -> Midi_State t -> IO r
st_access_pitch_bend f = st_access (f . st_pb)

st_access_usr :: (t -> u) -> Midi_State t -> IO u
st_access_usr f = st_access (f . st_usr)

st_read_note :: Midi_State t -> (Midi_Channel, Midi_Note_Number) -> IO (Maybe Midi_Velocity)
st_read_note st (ch, k) = st_access_km (Map.lookup (ch, k)) st

st_read_cc :: Midi_State t -> (Midi_Channel, Midi_CC_Number) -> IO Midi_CC_Value
st_read_cc st (ch, k) = st_access_cc (Map.findWithDefault 0 (ch, k)) st

st_read_pc :: Midi_State t -> Midi_Channel -> IO Midi_Program
st_read_pc st ch = st_access_pc (Map.findWithDefault 0 ch) st

st_chord :: Midi_State t -> IO [Midi_Note_Number]
st_chord = st_access_km (map (snd . fst) . Map.toAscList)

-- | Pitch bend with linear map to indicated range.
st_read_pitch_bend :: (Double, Double) -> Midi_State t -> Midi_Channel -> IO Double
st_read_pitch_bend rng st ch =
  let f = Sc3.linlin_hs (0, 16383) rng . fromIntegral . Map.findWithDefault 0 ch
  in st_access_pitch_bend f st

-- * Monitor incoming midi

st_recv_f :: Midi_State t -> Type.Channel_Voice_Message Type.Byte -> IO (Midi_State t)
st_recv_f st msg = do
  case msg of
    Type.Note_Off ch d1 _ -> st_edit_km (ch, d1, 0) st
    Type.Note_On ch d1 d2 -> st_edit_km (ch, d1, d2) st
    Type.Control_Change ch d1 d2 -> st_edit_cc (ch, d1, d2) st
    Type.Program_Change ch d1 -> st_edit_pc (ch, d1) st
    Type.Pitch_Bend ch d1 d2 -> st_edit_pb (ch, Common.bits_7_join_le (d1, d2)) st
    _ -> print msg
  return st

-- | Maintain state, call /f/ whenever state is modified.
st_run_usr :: t -> (Midi_State t -> IO (Midi_State t)) -> IO (Midi_State t, ThreadId)
st_run_usr usr_st usr_f = do
  v <- newMVar (Midi_State_T Map.empty Map.empty Map.empty Map.empty usr_st)
  let recv_f _fd st msg = st_recv_f st msg >>= usr_f
  th <- forkIO (Server.run_midi (\_ -> return v) recv_f)
  return (v, th)

-- | 'st_run_usr' with 'return'.
st_run :: IO (Midi_State (), ThreadId)
st_run = st_run_usr () return

{-
(st,th) <- st_run
st_chord st
readMVar st
st_read_note st 55
st_read_cc st 0
st_read_pitch_bend (0,1) st
killThread th
-}
