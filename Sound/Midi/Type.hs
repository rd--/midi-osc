{- | Midi types.
     The model has concrete type aliases for the 8-bit Status value and it's two 4-bit components (all are Int).
     The data elements (note number, velocity & so on) are given by a type parameter.
     This allows the bit operations (and the Bits class qualifier) required for the Status data to not colour the data type.
     In particular the types here can be (and are) used as containers for fractional midi data.
     If necessary a second type parameter could be added for the Status type, but it's not been neccesary yet.
-}
module Sound.Midi.Type where

import Data.Bits {- base -}

import qualified Sound.Sc3.Common.Math as Sc3 {- hsc3 -}

import Sound.Midi.Common {- midi-osc -}

-- * Types

-- | 4-bit word (0-15)
type Nibble = Int

-- | 8-bit word (0-255)
type Byte = Int

-- | Cast Byte to Int (identity)
byte_to_int :: Byte -> Int
byte_to_int = id

-- | 7-bit word (0-127)
type ByteWithParityBit = Int

-- | Message type values are 4-bit (0-15, 0x0-0xF).
type Message_Type = Nibble

-- | Channel values are 4-bit (0-15, 0x0-0xF).
type Channel = Nibble

-- | Status values are 8-bit (message-type & channel).
type Status = Byte

{- | Extract four-bit channel number from status byte.

>>> status_ch 0x9A == 0xA
True

>>> 0x9A .&. 0x0F == 0xA
True
-}
status_ch :: (Num t, Bits t) => t -> t
status_ch x = x .&. 0x0F

{- | Extract four-bit message-type from status byte.

>>> status_ty 0x9A == 0x9
True

>>> shiftR 0x9A 4 == 0x9
True
-}
status_ty :: Bits t => t -> t
status_ty = flip shiftR 4

{- | Separate status byte into low (channel) and high (message-type) 4-bit values.

>>> status_ch_ty 0x9A == (0xA,0x9)
True
-}
status_ch_ty :: (Num t, Bits t) => t -> (t, t)
status_ch_ty x = (status_ch x, status_ty x)

{- | Inverse of 'status_ch_ty'.

>>> status_combine (0xA,0x9) == 0x9A
True
-}
status_combine :: Bits a => (a, a) -> a
status_combine (ch, ty) = ch .|. shiftL ty 4

{- | Status values have the most-signficant bit set.

>>> map is_status [0x7F,0x80]
[False,True]
-}
is_status :: Bits t => t -> Bool
is_status w = testBit w 7

-- | Midi key number (0-127)
type Key = ByteWithParityBit

-- | Midi velocity (0-127)
type Velocity = ByteWithParityBit

{- | Channel voice messages have status 0x8 through 0xE.

>>> map is_channel_voice_message [0x81,0x90,0xF0]
[True,True,False]
-}
is_channel_voice_message :: (Ord n, Bits n, Num n) => n -> Bool
is_channel_voice_message st = let n = status_ty st in n >= 0x8 && n < 0xF

-- | System messages have status 0xF.
is_system_message :: (Eq n, Bits n, Num n) => n -> Bool
is_system_message st = status_ty st == 0xF

-- | System common messages have status 0xF0 through 0xF7.
is_system_common_message :: (Ord n, Num n) => n -> Bool
is_system_common_message st = st >= 0xF0 && st < 0xF8

-- | System real-time messages have status 0xF8 through 0xFF.
is_system_real_time_message :: (Ord n, Num n) => n -> Bool
is_system_real_time_message st = st >= 0xF8

-- | System-exclusive packet, with first byte of 0xF0 and last of 0xF7.
type SysEx d = [d]

-- | SysEx predicate, ie. is first byte 0xFO and last 0xF7.
is_system_exclusive :: (Eq d, Num d) => SysEx d -> Bool
is_system_exclusive d =
  case d of
    [] -> False
    0xF0 : _ -> last d == 0xF7
    _ -> False

-- | Segment sequence into SysEx messages, filter non-sysex (ie. un-terminated).
sysex_segment :: (Eq d, Num d) => [d] -> [SysEx d]
sysex_segment = filter is_system_exclusive . segment_at 0xF0

-- * Channel Voice Messages  <http://www.midi.org/techspecs/midimessages.php>

-- | Enumeration of Channel Voice Message types.
data Channel_Voice_Message_T
  = -- | 0x8
    Note_Off_T
  | -- | 0x9
    Note_On_T
  | -- | 0xA
    Polyphonic_Key_Pressure_T
  | -- | 0xB
    Control_Change_T
  | -- | 0xC
    Program_Change_T
  | -- | 0xD
    Channel_Aftertouch_T
  | -- | 0xE
    Pitch_Bend_T
  deriving (Eq, Enum, Bounded, Show)

{- | Table mapping Channel Voice Message types to 7-bit controller numbers.

>>> map fst channel_voice_message_tbl == [minBound .. maxBound]
True
-}
channel_voice_message_tbl :: (Num n, Enum n) => [(Channel_Voice_Message_T, n)]
channel_voice_message_tbl = zip [minBound .. maxBound] [0x8 ..]

{- | Channel Voice Messages
  <http://www.midi.org/techspecs/midimessages.php>

Channel voice messages have the channel number, not the status byte, in the first position.
-}
data Channel_Voice_Message d
  = -- | 0x80 (note-number, velocity)
    Note_Off Channel d d
  | -- | 0x90 (note-number, velocity)
    Note_On Channel d d
  | -- | 0xA0 (note-number, pressure)
    Polyphonic_Key_Pressure Channel d d
  | -- | 0xB0 (controller-number, controller-value)
    Control_Change Channel d d
  | -- | 0xC0 (program-number)
    Program_Change Channel d
  | -- | 0xD0 (pressure-value)
    Channel_Aftertouch Channel d
  | -- | 0xE0 (lsb-7,msb-7)
    Pitch_Bend Channel d d
  deriving (Eq, Show)

-- | System Common Message
data System_Common_Message d
  = -- | 0xF0 (sysex-data, including 0xF0 and 0xF7)
    System_Exclusive (SysEx d)
  | -- | 0xF1 (message-type, values)
    Midi_Time_Code_Quarter_Frame d d
  | -- | 0xF2 (lsb, msb)
    Song_Position_Pointer d d
  | -- | 0xF3
    Song_Select d
  | -- | 0xF4
    Undefined_F4
  | -- | 0xF5
    Undefined_F5
  | -- | 0xF6
    Tune_Request
  | -- | 0xF7
    End_of_Exclusive
  deriving (Eq, Show)

-- | System Real-Time Messages
data System_Real_Time_Message
  = -- | 0xF8
    Rt_Timing_Clock
  | -- | 0xF9
    Rt_Undefined_F9
  | -- | 0xFA
    Rt_Start
  | -- | 0xFB
    Rt_Continue
  | -- | 0xFC
    Rt_Stop
  | -- | 0xFD
    Rt_Undefined_FD
  | -- | 0xFE
    Rt_Active_Sensing
  | -- | 0xFF
    Rt_Reset
  deriving (Eq, Show)

-- | Sum type for the three types of Midi messages.
data Midi_Message d
  = Cvm (Channel_Voice_Message d)
  | Scm (System_Common_Message d)
  | Srtm System_Real_Time_Message
  deriving (Eq, Show)

-- * Parsers

{- | Parse 'Channel_Voice_Message'.
  Note: does not generate 'Note_On' with 0 velocity, translates to 'Note_Off'
-}
parse_channel_voice_message :: (Eq d, Bits d, Real d) => [d] -> Channel_Voice_Message d
parse_channel_voice_message d =
  case d of
    [st, d1, d2] ->
      let (ch, ty) = status_ch_ty (real_to_int st)
      in case ty of
          0x8 -> Note_Off ch d1 d2
          0x9 -> if d2 == 0 then Note_Off ch d1 0 else Note_On ch d1 d2
          0xA -> Polyphonic_Key_Pressure ch d1 d2
          0xB -> Control_Change ch d1 d2
          0xC -> Program_Change ch d1
          0xD -> Channel_Aftertouch ch d1
          0xE -> Pitch_Bend ch d1 d2
          _ -> error "parse_channel_voice_message: message-type?"
    _ -> error "parse_channel_voice_message: packet-length?"

-- | Parse 'System_Common_Message'.
parse_system_common_message :: (Eq d, Num d) => [d] -> System_Common_Message d
parse_system_common_message d =
  case d of
    0xF0 : _ ->
      if is_system_exclusive d
        then System_Exclusive d
        else error "parse_system_common_message: sysex?"
    [0xF1, d1, d2] -> Midi_Time_Code_Quarter_Frame d1 d2
    [0xF2, d1, d2] -> Song_Position_Pointer d1 d2
    [0xF3, d1] -> Song_Select d1
    [0xF4] -> Undefined_F4
    [0xF5] -> Undefined_F5
    [0xF6] -> Tune_Request
    [0xF7] -> End_of_Exclusive
    _ -> error "parse_system_common_message?"

-- | Parse 'System_Real_Time_Message'.
parse_system_real_time_message :: (Eq d, Num d) => [d] -> System_Real_Time_Message
parse_system_real_time_message d =
  case d of
    [0xF8] -> Rt_Timing_Clock
    [0xF9] -> Rt_Undefined_F9
    [0xFA] -> Rt_Start
    [0xFB] -> Rt_Continue
    [0xFC] -> Rt_Stop
    [0xFD] -> Rt_Undefined_FD
    [0xFE] -> Rt_Active_Sensing
    [0xFF] -> Rt_Reset
    _ -> error "parse_system_real_time_message?"

-- | Parse 'Midi_Message'.
parse_midi_message :: (Eq d, Bits d, Ord d, Real d) => [d] -> Midi_Message d
parse_midi_message d =
  case d of
    [] -> error "parse_midi_message: null?"
    st : _ ->
      if is_channel_voice_message st && length d == 3
        then Cvm (parse_channel_voice_message d)
        else
          if is_system_common_message st
            then Scm (parse_system_common_message d)
            else
              if is_system_real_time_message st && length d == 1
                then Srtm (parse_system_real_time_message d)
                else error "parse_midi_message?"

-- * Cvm

-- | Three value channel voice message, (status,data-1,data-2)
type Cvm3 d = (Status, d, d)

-- | Four value channel voice message, (message-type,channel,data-1,data-2)
type Cvm4 d = (Message_Type, Channel, d, d)

{- | Run 'is_channel_voice_message'.

>>> cvm3_verify (144,73,13)
True
-}
cvm3_verify :: Cvm3 d -> Bool
cvm3_verify (st, _, _) = is_channel_voice_message st

-- | Run 'cvm3_verify'.
cvm3_validate :: Cvm3 d -> Maybe (Cvm3 d)
cvm3_validate x = if cvm3_verify x then Just x else Nothing

-- | Apply /f/ at /d1/ and /d2/.
cvm3_coerce :: (t -> u) -> Cvm3 t -> Cvm3 u
cvm3_coerce f (st, d1, d2) = (st, f d1, f d2)

-- | (st,d1,d2) -> [st,d1,d2]
cvm3_to_list :: (Status -> t) -> Cvm3 t -> [t]
cvm3_to_list f (st, d1, d2) = [f st, d1, d2]

{- | Separate status byte into high (command) and low (channel) 4-bit values.

>>> cvm3_to_cvm4 (0x90,60,64) == (0x9,0,60,64)
True
-}
cvm3_to_cvm4 :: Cvm3 d -> Cvm4 d
cvm3_to_cvm4 (st, d1, d2) = (status_ty st, status_ch st, d1, d2)

-- | Inverse of 'cvm3_to_cvm4'.
cvm4_to_cvm3 :: Cvm4 d -> Cvm3 d
cvm4_to_cvm3 (ty, ch, d1, d2) = (status_combine (ch, ty), d1, d2)

-- * Types, derivations

-- | Derive pitch bend value as 'RealFrac' in indicated linear range.
pitch_bend :: (Real r, RealFrac f) => (f, f) -> (r, r) -> f
pitch_bend (lo, hi) (d1, d2) =
  let d = fbits_7_join_le (realToFrac d1, realToFrac d2)
  in Sc3.linlin_hs (0, 16383) (lo, hi) d

-- * Encoding & Decoding

{- | Encoding 'Channel_Voice_Message' to 'Cvm4'.

>>> cvm_to_cvm4 (Control_Change 0 16 127) == (0xB,0,16,127)
True
-}
cvm_to_cvm4 :: Num t => Channel_Voice_Message t -> Cvm4 t
cvm_to_cvm4 m =
  case m of
    Channel_Aftertouch ch d1 -> (0xD, ch, d1, 0)
    Control_Change ch d1 d2 -> (0xB, ch, d1, d2)
    Note_On ch d1 d2 -> (0x9, ch, d1, d2)
    Note_Off ch d1 d2 -> (0x8, ch, d1, d2)
    Polyphonic_Key_Pressure ch d1 d2 -> (0xA, ch, d1, d2)
    Program_Change ch d1 -> (0xC, ch, d1, 0)
    Pitch_Bend ch d1 d2 -> (0xE, ch, d1, d2)

{- | 'Cvm3' variant.

>>> cvm_to_cvm3 (Control_Change 0 16 127) == (0xB0,16,127)
True

>>> cvm_to_cvm3 (Program_Change 0 15) == (0xC0,15,0)
True
-}
cvm_to_cvm3 :: Num t => Channel_Voice_Message t -> Cvm3 t
cvm_to_cvm3 = cvm4_to_cvm3 . cvm_to_cvm4

{- | Byte sequence decoding for 'Channel_Voice_Message'.  An @0x9@ message
with velocity of zero is decoded as a 'Note_Off' message.
-}
cvm4_to_cvm :: (Num t, Bits t) => Cvm4 t -> Channel_Voice_Message t
cvm4_to_cvm m =
  case m of
    (0x8, ch, d1, d2) -> Note_Off ch d1 d2
    (0x9, ch, d1, 0) -> Note_Off ch d1 0 -- do not generate Note_On with 0 velocity
    (0x9, ch, d1, d2) -> Note_On ch d1 d2
    (0xA, ch, d1, d2) -> Polyphonic_Key_Pressure ch d1 d2
    (0xB, ch, d1, d2) -> Control_Change ch d1 d2
    (0xC, ch, d1, _) -> Program_Change ch d1
    (0xD, ch, d1, _) -> Channel_Aftertouch ch d1
    (0xE, ch, d1, d2) -> Pitch_Bend ch d1 d2
    _ -> error "cvm4_to_cvm: not channel message?"

{- | 'Cvm3' variant.

>>> cvm3_to_cvm (0xB0,16,127)
Control_Change 0 16 127
-}
cvm3_to_cvm :: (Num t, Bits t) => Cvm3 t -> Channel_Voice_Message t
cvm3_to_cvm = cvm4_to_cvm . cvm3_to_cvm4

{- | List variant of 'cvm_to_cvm3'.

>>> encode_channel_voice_message (Program_Change 0 15) == [0xC0,15,0]
True
-}
encode_channel_voice_message :: Num d => Channel_Voice_Message d -> [d]
encode_channel_voice_message m = let (st, d1, d2) = cvm_to_cvm3 m in [int_to_num st, d1, d2]

encode_system_common_message :: Num d => System_Common_Message d -> [d]
encode_system_common_message scm =
  case scm of
    System_Exclusive pkt -> pkt
    Midi_Time_Code_Quarter_Frame d1 d2 -> [0xF1, d1, d2]
    Song_Position_Pointer d1 d2 -> [0xF2, d1, d2]
    Song_Select d1 -> [0xF3, d1]
    Undefined_F4 -> [0xF4]
    Undefined_F5 -> [0xF5]
    Tune_Request -> [0xF6]
    End_of_Exclusive -> [0xF7]

encode_system_real_time_message :: Num d => System_Real_Time_Message -> [d]
encode_system_real_time_message srtm =
  case srtm of
    Rt_Timing_Clock -> [0xF8]
    Rt_Undefined_F9 -> [0xF9]
    Rt_Start -> [0xFA]
    Rt_Continue -> [0xFB]
    Rt_Stop -> [0xFC]
    Rt_Undefined_FD -> [0xFD]
    Rt_Active_Sensing -> [0xFE]
    Rt_Reset -> [0xFF]

encode_midi_message :: Num d => Midi_Message d -> [d]
encode_midi_message mm =
  case mm of
    Cvm x -> encode_channel_voice_message x
    Scm x -> encode_system_common_message x
    Srtm x -> encode_system_real_time_message x

-- * Time

-- | Timestamps are real-valued seconds.
type Time_Stamp = Double
