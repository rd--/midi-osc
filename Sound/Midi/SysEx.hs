-- | System Exclusive (SysEx) Messages
module Sound.Midi.SysEx where

import qualified Sound.Midi.Constant as Constant {- midi-osc -}
import qualified Sound.Midi.Type as Midi {- midi-osc -}

-- * Universal

--   <https://www.midi.org/specifications-old/item/table-4-universal-system-exclusive-messages>
--   <http://midi.teragonaudio.com/tech/midispec.htm>

-- | General Information ; Identity Request ; <http://midi.teragonaudio.com/tech/midispec/identity.htm>
identity_request_sysex_on :: Num i => i -> Midi.SysEx i
identity_request_sysex_on channel =
  [ Constant.k_SysEx_Status
  , Constant.k_Non_Real_Time
  , channel
  , Constant.k_General_Information
  , Constant.k_Identity_Request
  , Constant.k_SysEx_End
  ]

{- | Identity Request, Disregard Channel

>>> identity_request_sysex
[240,126,127,6,1,247]
-}
identity_request_sysex :: Num i => Midi.SysEx i
identity_request_sysex = identity_request_sysex_on Constant.k_Disregard_Channel

type Identity_Reply t = (t, (t, t, t), (t, t), (t, t), (t, t, t, t))

{- | General Information ; Identity Reply

m1,m2,m3 = Manufacturer Id (If m1 is 0x00 then Id is [m2,m3] else it is m1.)
f1,f2 = Family Id.
p1,p2 = Model/Product Id.
v1,v2,v2,v3 = Version Id.
-}
identity_reply_sysex_on :: (Eq i, Num i) => Identity_Reply i -> Midi.SysEx i
identity_reply_sysex_on (ch, (m1, m2, m3), (f1, f2), (p1, p2), (v1, v2, v3, v4)) =
  concat
    [
      [ Constant.k_SysEx_Status
      , Constant.k_Non_Real_Time
      , ch
      , Constant.k_General_Information
      , Constant.k_Identity_Reply
      ]
    , if m1 == 0 then [m2, m3] else [m1]
    ,
      [ f1
      , f2
      , p1
      , p2
      , v1
      , v2
      , v3
      , v4
      , Constant.k_SysEx_End
      ]
    ]

identity_reply_sysex_parse :: (Eq i, Num i) => Midi.SysEx i -> Maybe (Identity_Reply i)
identity_reply_sysex_parse sysEx =
  case sysEx of
    [0xF0, 0x7E, ch, 0x06, 0x02, m1, f1, f2, p1, p2, v1, v2, v3, v4, 0xF7] ->
      Just (ch, (m1, 0, 0), (f1, f2), (p1, p2), (v1, v2, v3, v4))
    [0xF0, 0x7E, ch, 0x06, 0x02, 0x00, m2, m3, f1, f2, p1, p2, v1, v2, v3, v4, 0xF7] ->
      Just (ch, (0x00, m2, m3), (f1, f2), (p1, p2), (v1, v2, v3, v4))
    _ -> Nothing

-- | Device Control ; Master Volume ; <http://midi.teragonaudio.com/tech/midispec/mastrvol.htm>
master_volume_sysex :: Num i => (i, i) -> Midi.SysEx i
master_volume_sysex (d1, d2) =
  [ Constant.k_SysEx_Status
  , Constant.k_Real_Time
  , Constant.k_Disregard_Channel
  , Constant.k_Device_Control
  , Constant.k_Master_Volume
  , d1
  , d2
  , Constant.k_SysEx_End
  ]
