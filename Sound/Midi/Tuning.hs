{- | Midi Tuning Standard

<https://www.fumph.com/ensoniq_mr/mrsysex.txt>
-}
module Sound.Midi.Tuning where

import Data.Bits {- base -}
import Data.List {- base -}

import qualified Data.List.Split as Split {- split -}

import qualified Sound.Midi.Common as Common {- midi-osc -}
import Sound.Midi.Type {- midi-osc -}

-- * Mts Frequency

{- | Cents 14-bit un-signed (msb,lsb)
     ie. the cents range (0.0,100.0) maps to (0x0000,0x3FFF) (2^14 - 1 == 0x3FFF)
     ie. the resolution in cents is 100 / 214 ~= 0.0061
-}
type Mts_Cents = (ByteWithParityBit, ByteWithParityBit)

-- | Frequency data format (midi-note-number,cents).  Cents is un-signed.
type Mts_Frequency = (Key, Mts_Cents)

{- | Decode 'Mts_Cents' to 14-bit valued stored as 'Int'.

>>> map mts_cents_u16 [(0,0),(0,0x7F),(0x01,0),(0x7F,0),(0x7F,0x7F)]
[0,127,128,16256,16383]
-}
mts_cents_u16 :: Mts_Cents -> Int
mts_cents_u16 (p, q) = Common.bits_7_join_le (q, p)

-- | Reserved value to indicate no change to the existing note tuning
mts_frequency_no_change :: Mts_Frequency
mts_frequency_no_change = (0x7F, (0x7F, 0x7F))

{- | Convert MTS 14-bit (msb,lsb) cents value to fractional cents.

>>> mts_frequency_cents_decode (97,42)
76.04223890618324

>>> map mts_frequency_cents_decode [(0,0),(0,1)]
[0.0,6.1038881767686015e-3]

>>> map (round . mts_frequency_cents_decode) [(0x7F,0),(0,0x7F)]
[99,1]
-}
mts_frequency_cents_decode :: Mts_Cents -> Double
mts_frequency_cents_decode c = fromIntegral (mts_cents_u16 c) * (100 / 0x3FFF)

{- | Convert fractional cents to MTS 14-bit (msb,lsb) cents value.

>>> mts_frequency_cents_encode 76.04223890618324
(97,42)

>>> map mts_frequency_cents_encode [99,1]
[(126,91),(1,35)]

>>> map mts_frequency_cents_encode [0,25,50,75,100]
[(0,0),(31,127),(63,127),(95,127),(127,127)]
-}
mts_frequency_cents_encode :: Double -> Mts_Cents
mts_frequency_cents_encode c =
  let swap (p, q) = (q, p)
  in swap (Common.bits_14_sep_le (floor (c * (0x3FFF / 100))))

{- | Convert from Mts_Frequency to (midi-note-number,cents-detune).

>>> mts_frequency_to_midi_detune (60,(64,0))
(60,50.003051944088384)
-}
mts_frequency_to_midi_detune :: Mts_Frequency -> (Int, Double)
mts_frequency_to_midi_detune (d1, c) = (fromIntegral d1, mts_frequency_cents_decode c)

{- | Convert from Mts_Frequency to fractional midi-note-number.

>>> mts_frequency_to_fmidi (60,(0,0))
60.0

>>> mts_frequency_to_fmidi (60,(63,127))
60.499969480559116

>>> mts_frequency_to_fmidi (60,(64,0))
60.500030519440884

>>> mts_frequency_to_fmidi (60,(127,127))
61.0
-}
mts_frequency_to_fmidi :: Mts_Frequency -> Double
mts_frequency_to_fmidi (d1, c) = fromIntegral d1 + (mts_frequency_cents_decode c / 100)

-- | Convert from (midi-note-number,cents-detune) to 'Mts_Frequency'.
midi_detune_to_mts_frequency :: (Int, Double) -> Mts_Frequency
midi_detune_to_mts_frequency (mnn, cents) = (fromIntegral mnn, mts_frequency_cents_encode cents)

{- | Convert from fractional midi-note-number to 'Mts_Frequency'.

>>> fmidi_to_mts_frequency 60
(60,(0,0))

>>> fmidi_to_mts_frequency 60.50006103515625
(60,(64,0))

>>> fmidi_to_mts_frequency 60.499969480559116
(60,(63,127))

>>> map (fst . snd . fmidi_to_mts_frequency) [60,60.1 .. 61.0]
[0,12,25,38,51,63,76,89,102,115,0]
-}
fmidi_to_mts_frequency :: Double -> Mts_Frequency
fmidi_to_mts_frequency x =
  let mnn = floor x
      cents = (x - fromIntegral mnn) * 100
  in midi_detune_to_mts_frequency (mnn, cents)

-- * Mts Nrt (2.5)

-- | Mts checksum calculation, XOR of all data, then AND 0x7F.
mts_checksum :: (Bits t, Num t) => [t] -> t
mts_checksum = (.&.) 0x7F . foldl1 xor

-- | Flatten 'Mts_Frequency' to three-element list.
mts_frequency_to_list :: Mts_Frequency -> [Byte]
mts_frequency_to_list (d1, (d2, d3)) = [d1, d2, d3]

-- | Inverse of 'mts_frequency_to_list'.
mts_frequency_from_list :: [Byte] -> Mts_Frequency
mts_frequency_from_list l =
  case l of
    [d1, d2, d3] -> (d1, (d2, d3))
    _ -> error "mts_frequency_from_list?"

{- | Truncate or extend string to 16 characters.

>>> map mts_name ["ET12",repeat '.']
["ET12            ","................"]
-}
mts_name :: String -> String
mts_name x = take 16 (x ++ repeat ' ')

-- | device-id to send to all devices.
mts_all_devices :: ByteWithParityBit
mts_all_devices = 0x7F

-- | (device-id,tuning-id,tuning-name,frequency-sequence)
type Mts_Nrt = (Byte, Byte, String, [Mts_Frequency])

{- | Universal MIDI Bulk Tuning Dump Reply Message (408 bytes, sub-ID 08-01, introduced in 1992)

> let syx = mts_nrt_sysex (mts_all_devices,0,mts_name "ET12",mts_et12_freq)
-}
mts_nrt_sysex :: Mts_Nrt -> [Byte]
mts_nrt_sysex (dev_id, tun_id, tun_nm, freq) =
  let nm_dat = if length tun_nm == 16 then map fromEnum tun_nm else error "mts_nrt_sysex: name?"
      freq_dat = concatMap mts_frequency_to_list freq
      dat = [0x7E, dev_id, 0x08, 0x01, tun_id] ++ nm_dat ++ freq_dat
      chk = if length dat == 405 then mts_checksum dat else error "mts_nrt_sysex: dat?"
  in 0xF0 : dat ++ [chk, 0xF7]

-- | Inverse of 'mts_nrt_sysex'.
mts_nrt_sysex_decode :: [Byte] -> Mts_Nrt
mts_nrt_sysex_decode syx =
  if length syx /= 408
    then error "mts_nrt_sysex_decode: 408?"
    else case syx of
      0xF0 : 0x7E : dev_id : 0x08 : 0x01 : tun_id : syx_dat ->
        let nm_dat = take 16 syx_dat
            freq_dat = take (128 * 3) (drop 16 syx_dat)
        in (dev_id, tun_id, map toEnum nm_dat, map mts_frequency_from_list (Split.chunksOf 3 freq_dat))
      _ -> error "mts_nrt_sysex_decode?"

-- | 'mts_nrt_sysex_decode' SYSEX at named file.
mts_nrt_sysex_load :: FilePath -> IO [Mts_Nrt]
mts_nrt_sysex_load = fmap (map mts_nrt_sysex_decode . sysex_segment) . Common.bytes_load

-- | The 127 ET12 frequencies.
mts_et12_freq :: [Mts_Frequency]
mts_et12_freq = map (\k -> (k, (0, 0))) [0 .. 127]

-- * Mts Rt (2.6)

-- | (key-number,mts-frequency)
type Mts_Tuning = (Byte, Mts_Frequency)

-- | Flatten 'Mts_Tuning' to four-element list.
mts_tuning_to_list :: Mts_Tuning -> [Byte]
mts_tuning_to_list (k, (d1, (d2, d3))) = [k, d1, d2, d3]

{- | 2.6 Universal MIDI Single Note Tuning Change Message

>>> mts_realtime_sysex 0 0 [(60,(60,(64,0)))]
[240,127,0,8,2,0,1,60,60,64,0,247]

>>> [0xF0,0x7F,0x00,0x08,0x02,0x00,0x01,0x3C,0x3C,0x40,0x00,0xF7]
[240,127,0,8,2,0,1,60,60,64,0,247]
-}
mts_realtime_sysex :: Byte -> Byte -> [Mts_Tuning] -> [Byte]
mts_realtime_sysex dev_id tun_id freq =
  let dat = concatMap mts_tuning_to_list freq
  in [0xF0, 0x7F, dev_id, 0x08, 0x02, tun_id, genericLength freq] ++ dat ++ [0xF7]

{-

2.5	Universal MIDI Bulk Tuning Dump Reply Message

As a recent addition to the MIDI Specification and the Ensoniq product
line, the following command allows the entire RAM pitch table to be
redefined with a single message.  This is achieved by specifying the
semitone frequency data for each of the 128 MIDI key numbers using a
3-byte (21-bit) data word.  The first byte (7 bits) specifies the
nearest equal-tempered semitone below the frequency of the note.  The
next two bytes (14 bits) specify the fraction of 100 cents above the
semitone at which the frequency lies.  This format yields an effective
resolution of 100 cents / 214 = 0.0061 cents.  In addition, the value
of [7F 7F 7F] has been reserved to indicate a "No Tuning Change"
condition for a given MIDI key number.

	F0		System Exclusive Status Byte
	7E		Non Real Time Message Code

	nn		Device ID Number (0 to 127)
			or
	7F		All Channel Broadcast Code

	08		MIDI Tuning Standard Message Code
	01		Bulk Dump Reply Message Code

	tt		Tuning Program Number (0 to 127)
			(This is currently ignored by the MR)
	[aa] x 16	Tuning Program Name - 16 ASCII Characters
			(This is currently ignored by the MR)

	xx		Semitone Frequency Data for MIDI Key #0
	yy		MSB Fraction of Semitone Data for MIDI Key #0
	zz		LSB Fraction of Semitone Data for MIDI Key #0

	Repeat [xx,yy,zz] 127 More Times For MIDI Keys #1 to 127

	cc		Checksum = XOR of
			7E (Non Real Time Message Code)
			08 (MIDI Tuning Standard Message Code)
			01 (Bulk Dump Reply Message Code)
			tt (Tuning Program Number)
			All 128 sets of [xx,yy,zz]

	F7		End of System Exclusive

2.6	Universal MIDI Single Note Tuning Change Message

This message allows real-time, performance oriented adjustments to be
made to any tuning stored within the RAM pitch table.  If this message
is received during the playback of a note (between key-down and
key-up), the tuning change will take effect on the next note.
Although this message is intended for the alteration of a single note,
multiple tuning changes can be embedded in one message in order to
maximize bandwidth.

In addition, the MR’s response to this message has been extended to
allow a single tuning change to alter the entire pitch range.  If a
Single Note Tuning Change Message is sent to Tuning Program Number
127, and if the note is between middle C and an octave above (MIDI key
numbers 60 to 71 inclusive), the tuning change will be applied to all
notes in the RAM pitch table.

	F0		System Exclusive Status Byte
	7F		Real Time Message Code

	nn		Device ID Number (0 to 127)
		or
	7F		All Channel Broadcast Code

	08		MIDI Tuning Standard Message Code
	02		Note Change Message Code

	tt		Tuning Program Number (0 to 127)
			(This is currently ignored by the MR)

	cc		Number of Note Changes

	kk		MIDI Key Number (0 to 127)
	xx		Semitone Frequency Data for MIDI Key kk
	yy		MSB Fraction of Semitone Data for MIDI Key kk
	zz		LSB Fraction of Semitone Data for MIDI Key kk

	Repeat [kk,xx,yy,zz] to Equal Number of Note Changes Minus One

	F7		End of System Exclusive

-}
