-- | Midi Cc (Control-Change)
module Sound.Midi.Controller where

import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Sound.Midi.Type as Midi {- midi-osc -}

-- * Channel Mode Messages

-- | Enumeration of Channel Mode Messages types.
data Channel_Mode_Message_T
  = -- | 120
    All_Sound_Off_T
  | -- | 121
    Reset_All_Controllers_T
  | -- | 0x7A=122
    Local_Control_T
  | -- | 0x7B=123
    All_Notes_Off_T
  | -- | 0x7C=124
    Omni_Mode_Off_T
  | -- | 0x7D=125
    Omni_Mode_On_T
  | -- | 0x7E=126
    Mono_Mode_On_T
  | -- | 0x7F=127
    Poly_Mode_On_T
  deriving (Bounded, Enum, Eq, Show)

-- | Table of Channel Mode Messages types.
channel_mode_message_tbl :: (Enum t, Num t) => [(t, Channel_Mode_Message_T)]
channel_mode_message_tbl = zip [120 .. 127] [All_Sound_Off_T .. Poly_Mode_On_T]

-- | 'lookup' 'channel_mode_message_tbl'.
channel_mode_message_parse :: (Enum t, Eq t, Num t) => t -> Maybe Channel_Mode_Message_T
channel_mode_message_parse = flip lookup channel_mode_message_tbl

-- | Get /d1/ value for 'Channel_Mode_Message_T'.
channel_mode_message_encode :: (Enum t, Num t) => Channel_Mode_Message_T -> t
channel_mode_message_encode =
  fromMaybe (error "channel_mode_message_encode")
    . flip lookup (map (\(p, q) -> (q, p)) channel_mode_message_tbl)

-- | 'Channel_Mode_Message_T' to 'Channel_Voice_Message'
channel_mode_message :: (Enum t, Num t) => Midi.Channel -> Channel_Mode_Message_T -> t -> Midi.Channel_Voice_Message t
channel_mode_message ch ty = Midi.Control_Change ch (channel_mode_message_encode ty)

-- | 'channel_mode_message' of 'All_Notes_Off_T'
all_notes_off :: (Enum t, Num t) => Midi.Channel -> Midi.Channel_Voice_Message t
all_notes_off ch = channel_mode_message ch All_Notes_Off_T 0

-- | 'channel_mode_message' of 'All_Sound_Off_T'
all_sound_off :: (Enum t, Num t) => Midi.Channel -> Midi.Channel_Voice_Message t
all_sound_off ch = channel_mode_message ch All_Sound_Off_T 0

{- | Channel Mode Messages

Control_Change (0xB) messages implement Mode control by using reserved controller numbers 120-127.
-}
data Channel_Mode_Message d
  = -- | 120 (d = 0)
    All_Sound_Off Midi.Channel d
  | -- | 121 (d = 0)
    Reset_All_Controllers Midi.Channel d
  | -- | 122 (d = 0 | 127)
    Local_Control Midi.Channel d
  | -- | 123 (d = 0)
    All_Notes_Off Midi.Channel d
  | -- | 124 (d = 0)
    Omni_Mode_Off Midi.Channel d
  | -- | 125 (d = 0)
    Omni_Mode_On Midi.Channel d
  | -- | 126 (d = number-of-channels)
    Mono_Mode_On Midi.Channel d
  | -- | 127 (d = 0)
    Poly_Mode_On Midi.Channel d
  deriving (Eq, Show)

{- | Derive 'Channel_Mode_Message'

>>> channel_mode_message_derive (0x0,123,0)
All_Notes_Off 0 0
-}
channel_mode_message_derive :: (Eq t, Num t) => (Midi.Channel, t, t) -> Channel_Mode_Message t
channel_mode_message_derive (ch, d1, d2) =
  case d1 of
    120 -> All_Sound_Off ch d2
    121 -> Reset_All_Controllers ch d2
    122 -> Local_Control ch d2
    123 -> All_Notes_Off ch d2
    124 -> Omni_Mode_Off ch d2
    125 -> Omni_Mode_On ch d2
    126 -> Mono_Mode_On ch d2
    127 -> Poly_Mode_On ch d2
    _ -> error "channel_mode_message?"

-- | If 0xB control message with d1 in (120-127) make Channel_Mode_Message, else error.
cvm4_to_channel_mode_message :: (Eq t, Num t) => Midi.Cvm4 t -> Channel_Mode_Message t
cvm4_to_channel_mode_message (ty, ch, d1, d2) =
  case ty of
    0xB -> channel_mode_message_derive (ch, d1, d2)
    _ -> error "cvm4_to_channel_mode_message"

-- * Control Message Types

{- | Enumeration of control message types.
  'Control_Change' midi messages have, in some cases, commonly defined meanings.
  The table is SPARSE, undefined messages are omitted.

<https://www.midi.org/specifications-old/item/table-3-control-change-messages-data-bytes-2>
-}
data Control_Message_T
  = -- | 0x00, 14-bit LSB = 0x20
    Bank_Select_T
  | -- | 0x01, 14-bit LSB = 0x21
    Modulation_Wheel_T
  | -- | 0x02, 14-bit LSB = 0x22
    Breath_Controller_T
  | -- | 0x04, 14-bit LSB = 0x24
    Foot_Controller_T
  | -- | 0x05, 14-bit LSB = 0x25
    Portamento_Time_T
  | -- | 0x06, 14-bit LSB = 0x26
    Data_Entry_T
  | -- | 0x07, 14-bit LSB = 0x27
    Channel_Volume_T
  | -- | 0x08, 14-bit LSB = 0x28
    Balance_T
  | -- | 0x0A, 14-bit LSB = 0x2A
    Pan_T
  | -- | 0x0B, 14-bit LSB = 0x2B
    Expression_Controller_T
  | -- | 0x40=64 SUSTAIN
    Damper_Pedal_On_Off_T
  | -- | 0x41
    Portamento_On_Off_T
  | -- | 0x42
    Sostenuto_On_Off_T
  | -- | 0x43
    Soft_Pedal_On_Off_T
  | -- | 0x44=68
    Legato_Footswitch_T
  | -- | 0x45=69
    Hold_2_T
  | -- | 0x47==71  Timbre, Harmonic Intensity
    Resonance_T
  | -- | 0x48=72
    Release_Time_T
  | -- | 0x49=73
    Attack_Time_T
  | -- | 0x4A=74 (CutoffFrequency)
    Brightness_T
  | -- | 0x4B=75
    Decay_Time_T
  | -- | 0x4C=76
    Vibrato_Rate_T
  | -- | 0x4D=77
    Vibrato_Depth_T
  | -- | 0x4E=78
    Vibrato_Delay_T
  | -- | 0x4F=79
    Portamento_Control_T
  | -- | 0x5B=91
    Reverb_Send_Level_T
  | -- | 0x5C=92
    Tremolo_Depth_T
  | -- | 0x5D=93
    Chorus_Send_Level_T
  | -- | 0x5E=94
    Detune_Depth_T
  | -- | 0x5F=95
    Phaser_Depth_T
  | -- | 0x60=96
    Inrement_Data_T
  | -- | 0x61=97
    Decrement_Data_T
  | -- | 0x62=98
    NRPN_LSB_T
  | -- | 0x63=99
    NRPN_MSB_T
  | -- | 0x64=100
    RPN_LSB_T
  | -- | 0x65=101
    RPN_MSB_T
  deriving (Eq, Enum, Bounded, Show)

-- | Control Message data type.
data Control_Message d
  = -- | 0x00
    Bank_Select Midi.Channel d
  | -- | 0x01
    Modulation_Wheel Midi.Channel d
  | -- | 0x02
    Breath_Controller Midi.Channel d
  | -- | 0x04
    Foot_Controller Midi.Channel d
  | Portamento_Time Midi.Channel d
  | -- | 0x06
    Data_Entry Midi.Channel d
  | -- | 0x07
    Channel_Volume Midi.Channel d
  | -- | 0x08
    Balance Midi.Channel d
  | -- | 0x0A
    Pan Midi.Channel d
  | -- | 0x0B
    Expression_Controller Midi.Channel d
  | -- | 0x40=64
    Damper_Pedal_On_Off Midi.Channel d
  | -- | 0x41
    Portamento_On_Off Midi.Channel d
  | Sostenuto_On_Off
      Midi.Channel
      -- | 0x42
      d
  | -- | 0x43
    Soft_Pedal_On_Off Midi.Channel d
  | Legato_Footswitch Midi.Channel d
  | Hold_2 Midi.Channel d
  | Release_Time Midi.Channel d
  | Attack_Time Midi.Channel d
  | Brightness Midi.Channel d
  | Decay_Time Midi.Channel d
  | Vibrato_Rate Midi.Channel d
  | Vibrato_Depth Midi.Channel d
  | Vibrato_Delay Midi.Channel d
  | Portamento_Control Midi.Channel d
  | Reverb_Send_Level Midi.Channel d
  | Tremolo_Depth Midi.Channel d
  | Chorus_Send_Level Midi.Channel d
  | Detune_Depth Midi.Channel d
  | Phaser_Depth Midi.Channel d
  deriving (Eq, Show)

{- | Table mapping control message types to 7-bit controller numbers.

>>> import qualified Music.Theory.List as List
>>> List.group_ranges $ sort $ map snd control_message_tbl
[(0,2),(4,5),(7,8),(10,11),(64,69),(72,78),(84,84),(91,95)]

> map fst control_message_tbl
-}
control_message_tbl :: (Enum n, Num n) => [(Control_Message_T, n)]
control_message_tbl =
  let ix_grp = [[0 .. 2], [4, 5], [7, 8], [10, 11], [64 .. 69], [72 .. 78], [84], [91 .. 95]]
  in zip [minBound .. maxBound] (concat ix_grp)

{- | Erroring lookup at 'control_message_tbl'.

>>> control_message_from_enum Modulation_Wheel_T == 0x01
True
-}
control_message_from_enum :: (Enum n, Num n) => Control_Message_T -> n
control_message_from_enum =
  fromMaybe (error "control_message_from_enum")
    . flip lookup control_message_tbl

{- | Reverse lookup at 'control_message_tbl'.

>>> control_message_to_enum 0x01 == Just Modulation_Wheel_T
True
-}
control_message_to_enum :: (Eq n, Enum n, Num n) => n -> Maybe Control_Message_T
control_message_to_enum n = fmap fst (find ((==) n . snd) control_message_tbl)

{- | 'Control_Change' midi messages may, in some cases, have commonly defined meanings.

> control_message (0x0,0x01,0x53) == Modulation_Wheel 0x0 0x53
True
-}
control_message :: (Midi.Channel, Midi.Message_Type, t) -> Control_Message t
control_message (ch, ty, d) =
  case ty of
    0 -> Bank_Select ch d
    -- \^ 0x00
    1 -> Modulation_Wheel ch d
    -- \^ 0x01
    2 -> Breath_Controller ch d
    4 -> Foot_Controller ch d
    5 -> Portamento_Time ch d
    6 -> Data_Entry ch d
    -- \^ 0x06
    7 -> Channel_Volume ch d
    -- \^ 0x07
    8 -> Balance ch d
    -- \^ 0x08
    10 -> Pan ch d
    -- \^ 0x0A
    11 -> Expression_Controller ch d
    -- \^ 0x0B
    64 -> Damper_Pedal_On_Off ch d
    -- \^ SUSTAIN 0x40=64
    65 -> Portamento_On_Off ch d
    -- \^ 0x41
    66 -> Sostenuto_On_Off ch d
    -- \^ 0x42
    67 -> Soft_Pedal_On_Off ch d
    -- \^ 0x43
    68 -> Legato_Footswitch ch d
    69 -> Hold_2 ch d
    72 -> Release_Time ch d
    -- \^ 0x48
    73 -> Attack_Time ch d
    -- \^ 0x49
    74 -> Brightness ch d
    -- \^ 0x4A
    75 -> Decay_Time ch d
    -- \^ 0x4B
    76 -> Vibrato_Rate ch d
    -- \^ 0x4C
    77 -> Vibrato_Depth ch d
    -- \^ 0x4D
    78 -> Vibrato_Delay ch d
    -- \^ 0x4E
    84 -> Portamento_Control ch d
    91 -> Reverb_Send_Level ch d
    -- \^ 0x5B
    92 -> Tremolo_Depth ch d
    -- \^ 0x5C=92
    93 -> Chorus_Send_Level ch d
    -- \^ 0x5D=93
    94 -> Detune_Depth ch d
    -- \^ 0x5E=94
    95 -> Phaser_Depth ch d
    -- \^ 0x5F=95
    _ -> error "control_message?"
