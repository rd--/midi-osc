/* ALSA sequencer host.  Incoming events have source and destination
   address fields.  The model midi-osc provides requires that source
   and destination ports can be enumerated. */

#include <alsa/asoundlib.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>

#include "r-common/c/failure.h"
#include "r-common/c/int.h"
#include "r-common/c/print.h"

#include "r-common/c/alsa-seq-endpoint.c"
#include "r-common/c/alsa-seq-listener.c"
#include "r-common/c/alsa-seq-print.c"
#include "r-common/c/alsa-seq-send.c"

#include "host-alsa.h"

midi_host_t *
midi_host_alloc(void)
{
	midi_host_t *host = malloc(sizeof(midi_host_t));
	return host;
}

void midi_host_init(midi_host_t *host, void *server,
	midi_host_send_t send, midi_host_notify_t notify)
{
	host->seq = NULL;
	host->in = 0;
	host->out = 0;
	host->source = NULL;
	host->source_n = 0;
	host->destination = NULL;
	host->destination_n = 0;
	host->encoder = NULL;
	host->decoder = NULL;
	host->server = server;
	host->send = send;
	host->notify = notify;
	return;
}

int midi_host_number_of_sources(const midi_host_t *host)
{
	return host->source_n;
}

int midi_host_number_of_destinations(const midi_host_t *host)
{
	return host->destination_n;
}

/* If allocated free source and destination arrays.  Allocate places
   for all endpoints at `seq' and read the endpoint set to `e'. */

void midi_host_reset(midi_host_t *host)
{
	if (host->source) {
		free(host->source);
		host->source = NULL;
		host->source_n = 0;
	}
	if (host->destination) {
		free(host->destination);
		host->destination = NULL;
		host->destination_n = 0;
	}
	alsa_seq_endpoint_read(host->seq,
		&(host->source), &(host->source_n),
		&(host->destination), &(host->destination_n));
	alsa_seq_endpoint_connect_from(host->seq, host->in,
		host->source, host->source_n);
	alsa_seq_endpoint_connect_to(host->seq, host->in,
		host->destination, host->destination_n);
#if DEBUG
	alsa_seq_endpoint_print_set(host->seq, host->source, host->source_n);
	alsa_seq_endpoint_print_set(host->seq, host->destination, host->destination_n);
#endif
	return;
}

void midi_host_port_name(midi_host_t *host, bool source, int uid,
	char *device_name, char *port_name, int size)
{
	alsa_seq_endpoint_t e;
	if (source) {
		if (uid > host->source_n) {
			eprintf("%s: Illegal source UID, %d\n", __func__, uid);
			FAILURE;
		}
		e = host->source[uid];
	} else {
		if (uid > host->destination_n) {
			eprintf("%s: Illegal destination UID, %d\n", __func__, uid);
			FAILURE;
		}
		e = host->destination[uid];
	}
	alsa_seq_endpoint_name(host->seq, &e, device_name, port_name, size);
	return;
}

void midi_host_receiver(void *context, const snd_seq_event_t *event,
	const u8 *data, int data_n)
{
	midi_host_t *host;
	host = (midi_host_t *)context;
	int index;
	index = alsa_seq_endpoint_index(host->source,
		host->source_n,
		event->source.client,
		event->source.port);
	dprintf("%s: received packet: host=%p, evt=%p, indx=%d, data=%p, n=%d\n",
		__func__, host, event, index, data, data_n);
	host->send(host->server,
		index,
		(u8 *)data, (int)data_n,
		RECEIVE_MIDI);
}

void midi_host_open(midi_host_t *host, const char *client_name)
{
	int err;
	err = snd_seq_open(&(host->seq),
		"hw",
		SND_SEQ_OPEN_DUPLEX,
		0);
	if (err < 0) {
		eprintf("%s: snd_seq_open failed: %d\n", __func__, err);
		FAILURE;
	}
	snd_seq_set_client_name(host->seq, client_name);
	host->in = snd_seq_create_simple_port(host->seq,
		"In",
		SND_SEQ_PORT_CAP_WRITE,
		SND_SEQ_PORT_TYPE_MIDI_GENERIC);
	host->out = snd_seq_create_simple_port(host->seq,
		"Out",
		SND_SEQ_PORT_CAP_READ,
		SND_SEQ_PORT_TYPE_MIDI_GENERIC);
	host->queue = alsa_seq_make_default_queue(host->seq);
	snd_midi_event_new(ALSA_SEQUENCER_PACKET_LIMIT, &(host->encoder));
	snd_midi_event_new(ALSA_SEQUENCER_PACKET_LIMIT, &(host->decoder));
	snd_midi_event_no_status(host->decoder, 1);
	host->listener = alsa_seq_listener(host->seq,
		host->decoder,
		midi_host_receiver,
		host);
}

void midi_host_send_midi(midi_host_t *host,
	int id,
	const u8 *data, int size)
{
	if (id >= host->destination_n) {
		eprintf("%s: id=%d >= destination_n=%d\n", __func__, id, host->destination_n);
		return;
	}
	alsa_seq_endpoint_t e;
	e = host->destination[id];
	alsa_seq_send_to(host->seq, host->queue,
		host->out,
		e.client, e.port,
		host->encoder,
		(u8 *)data,
		size);
}

void midi_host_close(midi_host_t *host)
{
	snd_seq_close(host->seq);
}
