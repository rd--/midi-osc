#include <alsa/asoundlib.h>
#include <stdbool.h>
#include <stdint.h>

#include "r-common/c/alsa-seq-endpoint.h"

typedef void (*midi_host_send_t)(void *, int, const uint8_t *, int, int);
typedef void (*midi_host_notify_t)(void *, int);

typedef struct
{
	snd_seq_t *seq;
	int queue;
	int in;
	int out;
	alsa_seq_endpoint_t *source;
	int source_n;
	alsa_seq_endpoint_t *destination;
	int destination_n;
	snd_midi_event_t *encoder;
	snd_midi_event_t *decoder;
	void *server;
	midi_host_send_t send;
	midi_host_notify_t notify;
	pthread_t listener;
} midi_host_t;

#define ALSA_SEQUENCER_PACKET_LIMIT 8192
#define ALSA_SEQUENCER_MAX_PORTS 64

#include "host-common.h"
