#include <CoreAudio/HostTime.h>
#include <stdbool.h>
#include <stdint.h>

#include "../r-common/c/failure.h"
#include "../r-common/c/print.h"

#include "host-core-midi.h"

/* Make a CFString using the default allocator and MacRoman
   encoding. */

static CFStringRef
make_standard_cf_string(const char *str)
{
	CFAllocatorRef allocator;
	allocator = CFAllocatorGetDefault();

	int encoding;
	encoding = kCFStringEncodingMacRoman;

	return CFStringCreateWithCString(allocator, str, encoding);
}

/* Delete any existing input ports. */

static void
midi_host_delete_inputs(midi_host_t *host)
{
	if (host->in) {
		int i;
		for (i = 0; i < host->in_n; i++) {
			MIDIPortDispose(host->in[i]);
		}
		free(host->in);
		host->in = NULL;
		host->in_n = 0;
	}
}

/* Delete any existing output port and destination references. */

static void
midi_host_delete_outputs(midi_host_t *host)
{
	if (host->out) {
		MIDIPortDispose(host->out);
		host->out = NULL;
	}
	if (host->dst) {
		free(host->dst);
		host->dst = NULL;
		host->dst_n = 0;
	}
}

/* Receive an incoming midi packet.  According to the Apple manual the
   packet may contain more than one midi message... */

static void
midi_host_process_packet(midi_host_t *host, MIDIPacket *pkt, int uid)
{
	host->send(host->server,
		uid,
		(uint8_t *)pkt->data, (int)pkt->length,
		RECEIVE_MIDI);
}

/* Receive a set of incoming MIDI packets.  Defers processing to
   'midi_process_packet'.  */

static void
midi_host_read(const MIDIPacketList *pktlist,
	void *readProcRefCon,
	void *srcConnRefCon)
{
	midi_host_t *m;
	m = (midi_host_t *)readProcRefCon;

	MIDIPacket *pkt;
	pkt = (MIDIPacket *)pktlist->packet;

	int uid;
	uid = (int)srcConnRefCon;

	int i;
	for (i = 0; i < pktlist->numPackets; i++) {
		midi_host_process_packet(m, pkt, uid);
		pkt = MIDIPacketNext(pkt);
	}
}

/* Make input ports.  Existing ports are deleted.  An input port is
   created for each source in the system.  This allows for sources to
   be identified by the receiver. The refCon given to the port is an
   integer value identifying the port. */

static void
midi_host_make_inputs(midi_host_t *host)
{
	midi_host_delete_inputs(host);

	host->in_n = (int)MIDIGetNumberOfSources();
	host->in = malloc(host->in_n * sizeof(MIDIPortRef));
	dprintf("%s: There are %d sources\n", __func__, host->in_n);

	int i;
	for (i = 0; i < host->in_n; i++) {

		const int c_port_name_extent = 32;
		char c_port_name[c_port_name_extent];
		snprintf(c_port_name, c_port_name_extent, "In%d", i);

		CFStringRef cf_port_name;
		cf_port_name = make_standard_cf_string(c_port_name);

		OSStatus err;
		err = MIDIInputPortCreate(host->client,
			cf_port_name,
			midi_host_read,
			host,
			&(host->in[i]));
		CFRelease(cf_port_name);

		if (err) {
			eprintf("%s: Could not create input port: %d\n",
				__func__, (int)err);
			FAILURE;
		}
	}
}

/* Make an output port.  Any existing port is deleted.  The
   destination endpoints are collecterd and stored. */

static void
midi_host_make_outputs(midi_host_t *host)
{
	midi_host_delete_outputs(host);

	CFStringRef cf_port_name;
	cf_port_name = make_standard_cf_string("Out");

	OSStatus err;
	err = MIDIOutputPortCreate(host->client,
		cf_port_name,
		&(host->out));
	CFRelease(cf_port_name);

	host->dst_n = (int)MIDIGetNumberOfDestinations();
	host->dst = malloc(host->dst_n * sizeof(MIDIEndpointRef));
	dprintf("%s: There are %d destinations\n", __func__, host->dst_n);

	int i;
	for (i = 0; i < host->dst_n; i++) {
		host->dst[i] = MIDIGetDestination(i);
	}
}

/* Connect sources to the existing input ports. */

static void
midi_host_connect(midi_host_t *host)
{
	int i;
	for (i = 0; i < host->in_n; i++) {
		MIDIEndpointRef point;
		point = MIDIGetSource(i);
		MIDIPortConnectSource(host->in[i], point, (void *)i);
	}
}

/* Receive notification of host system status changes. */

static void
midi_host_notify(const MIDINotification *msg, void *refCon)
{
	midi_host_t *host;
	host = (midi_host_t *)refCon;
	host->notify(host->server, (int)msg->messageID);
}

/* Allocate structure storage. */

midi_host_t *
midi_host_alloc(void)
{
	midi_host_t *host = malloc(sizeof(midi_host_t));
	return host;
}

/* Initialize all fields at `host'. */

void midi_host_init(midi_host_t *host, void *server,
	midi_host_send_t send, midi_host_notify_t notify)
{
	host->client = NULL;
	host->in = NULL;
	host->in_n = 0;
	host->out = NULL;
	host->dst = NULL;
	host->dst_n = 0;
	host->server = server;
	host->send = send;
	host->notify = notify;
}

/* Return the number of host system MIDI sources. */

int midi_host_number_of_sources(const midi_host_t *host)
{
	return (int)MIDIGetNumberOfSources();
}

/* Return the number of host system MIDI destinations. */

int midi_host_number_of_destinations(const midi_host_t *host)
{
	return (int)MIDIGetNumberOfDestinations();
}

/* Reset midi inputs and re-connect sources. */

void midi_host_reset(midi_host_t *host)
{
	midi_host_make_inputs(host);
	midi_host_make_outputs(host);
	midi_host_connect(host);
}

/* Determine the the device and endpoint names for the source `uid'.
   If `src' is true the enpoint is a source, else a destination.  */

void midi_host_port_name(midi_host_t *host, bool src, int uid,
	char *c_device_name, char *c_end_name, int size)
{
	if (uid < 0 || uid >= host->in_n) {
		eprintf("%s: Illegal UID %d\n", __func__, uid);
		FAILURE;
	}

	MIDIEndpointRef endpoint;
	if (src) {
		endpoint = MIDIGetSource(uid);
	} else {
		endpoint = MIDIGetDestination(uid);
	}

	SInt32 sid;
	MIDIObjectGetIntegerProperty(endpoint, kMIDIPropertyUniqueID, &sid);

	MIDIEntityRef entity;
	OSStatus err;
	err = MIDIEndpointGetEntity(endpoint, &entity);

	CFStringRef cf_devive_name;
	CFStringRef cf_end_name;

	/* Virtual sources don't have entities. */

	if (err) {
		MIDIObjectGetStringProperty(endpoint,
			kMIDIPropertyName, &cf_devive_name);
		MIDIObjectGetStringProperty(endpoint,
			kMIDIPropertyName, &cf_end_name);
	} else {
		MIDIDeviceRef device;
		MIDIEntityGetDevice(entity, &device);
		MIDIObjectGetStringProperty(device,
			kMIDIPropertyName, &cf_devive_name);
		MIDIObjectGetStringProperty(endpoint,
			kMIDIPropertyName, &cf_end_name);
	}

	CFStringGetCString(cf_devive_name,
		c_device_name, size, kCFStringEncodingUTF8);
	CFStringGetCString(cf_end_name,
		c_end_name, size, kCFStringEncodingUTF8);
	CFRelease(cf_devive_name);
	CFRelease(cf_end_name);
}

/* Open a MIDI client.  This does not create an input port, or
   establish any connections.  */

void midi_host_open(midi_host_t *host, const char *client_name)
{
	CFStringRef str;
	str = make_standard_cf_string(client_name);

	OSStatus err;
	err = MIDIClientCreate(str, midi_host_notify, host, &(host->client));
	CFRelease(str);

	if (err) {
		eprintf("%s: Could not create MIDI client: %d\n",
			__func__, (int)err);
		FAILURE;
	}
}

void midi_host_close(midi_host_t *host)
{
	return;
}

void midi_host_send_midi(midi_host_t *host,
	int id,
	const uint8_t *data, int size)
{
	MIDIPacketList pkt_list_d;
	MIDIPacketList *pkt_list;
	pkt_list = &pkt_list_d;

	MIDIPacket *pkt;
	pkt = MIDIPacketListInit(pkt_list);

	uint64_t host_time;
	host_time = AudioGetCurrentHostTime();

	memcpy(pkt->data, data, size);
	pkt = MIDIPacketListAdd(pkt_list,
		sizeof(struct MIDIPacketList),
		pkt,
		(MIDITimeStamp)host_time,
		(ByteCount)size,
		data);

	OSStatus err;
	err = MIDISend(host->out, host->dst[id], pkt_list);
	if (err) {
		eprintf("%s: MIDISend failed: %d\n", __func__, (int)err);
	}
}
