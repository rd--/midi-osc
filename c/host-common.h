#include <stdbool.h>

#include "r-common/c/int.h"

#define RECEIVE_MIDI 0x0000001
#define RECEIVE_META 0x0000002
#define RECEIVE_ALL 0xFFFFFFF

midi_host_t *
midi_host_alloc(void);

void midi_host_init(midi_host_t *host, void *server,
	midi_host_send_t send, midi_host_notify_t notify);

int midi_host_number_of_sources(const midi_host_t *host);

int midi_host_number_of_destinations(const midi_host_t *host);

void midi_host_reset(midi_host_t *host);

void midi_host_port_name(midi_host_t *host, bool src, int uid,
	char *device_name, char *port_name, int size);

void midi_host_open(midi_host_t *host, const char *client_name);

void midi_host_send_midi(midi_host_t *host,
	int id, const u8 *data, int size);

void midi_host_close(midi_host_t *host);
