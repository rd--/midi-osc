#include <CoreMIDI/CoreMIDI.h>

typedef void (*midi_host_send_t)(void *, int, const uint8_t *, int, int);
typedef void (*midi_host_notify_t)(void *, int);

/* The server field is an opaque value that is passed as the first
   argument to the 'send' and 'notify' methods. */

typedef struct
{
	MIDIClientRef client;
	MIDIPortRef *in;
	int in_n;
	MIDIPortRef out;
	MIDIEndpointRef *dst;
	int dst_n;
	void *server;
	midi_host_send_t send;
	midi_host_notify_t notify;
} midi_host_t;

#include "host-common.h"
