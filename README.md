midi-osc
--------

[midi](http://www.midi.org/)
<->
[open sound control](http://opensoundcontrol.org/)

A translation daemon with support for
[ALSA](http://www.alsa-project.org/)
sequencer and
[CoreMidi](http://developer.apple.com/)
hosts.

## cli

midi-osc-
[daemon](?t=midi-osc&e=md/midi-osc-daemon.md) (December 2004),
[ctl](?t=midi-osc&e=md/ctl.md),
[pm](?t=midi-osc&e=md/pm.md),
[rtm](?t=midi-osc&e=md/rtm.md),
[sc3](?t=midi-osc&e=md/sc3.md),
[sc3-unit](?t=midi-osc&e=md/sc3-unit.md)

tested-with:

- [ghc](http://www.haskell.org/ghc/) 9.10.1
- [gcc](http://gcc.gnu.org/) 12.2.0
- [clang](https://clang.llvm.org/) 11.0.1

debian: libportmidi0 libportmidi-dev librtmidi5 librtmidi-dev

© [rohan drape](http://rohandrape.net/rd),
  2004-2024,
  [gpl](http://gnu.org/copyleft/)

initial announcement:
[2004-12-14/laa](http://lists.linuxaudio.org/pipermail/linux-audio-announce/2004-December/000501.html)

* * *

```
$ make doctest
Examples: 92  Tried: 92  Errors: 0  Failures: 0
$
```
