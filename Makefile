all:
	echo "midi-osc"

mk-cmd:
	(cd c ; make all install)
	(cd cmd ; make all install)

clean:
	rm -Rf dist dist-newstyle *~
	(cd c ; make clean)
	(cd cmd ; make clean)

push-all:
	r.gitlab-push.sh midi-osc

push-tags:
	r.gitlab-push.sh midi-osc --tags

indent:
	fourmolu -i Sound cmd

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound
